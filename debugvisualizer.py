from vscodedebugvisualizer import globalVisualizationFactory
from vscodedebugvisualizer.visualizer.NumpyVisualizer import NumpyVisualizer
from vscodedebugvisualizer.visualizer.MultiLineNumpyVisualizer import MultiLineNumpyVisualizer

from pyPhasesRecordloader import RecordSignal, Signal


class SignalVisualizer(NumpyVisualizer):
    def checkType(self, t):
        """checks if the given object `t` is an instance of Person"""
        return isinstance(t, Signal)

    def visualize(self, signal: Signal):
        return super().visualize(signal.signal)


globalVisualizationFactory.addVisualizer(SignalVisualizer())


class RecordSignalVisualizer(MultiLineNumpyVisualizer):
    def checkType(self, t):
        """checks if the given object `t` is an instance of Person"""
        return isinstance(t, RecordSignal)

    def visualize(self, signal: RecordSignal):
        lines = [s.signal for s in signal.signals]
        names = [s.name for s in signal.signals]
        self.labels = names
        self.maxChannels = 6
        self.dtype = "float16"
        return super().visualize(lines)


globalVisualizationFactory.addVisualizer(RecordSignalVisualizer())
