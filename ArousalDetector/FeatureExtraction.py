from typing import List

import numpy as np
from pyPhasesML import FeatureExtraction as pyPhasesFeatureExtraction
from pyPhasesML import ModelManager
from pyPhasesRecordloader import RecordSignal

from .DataAugmentation import DataAugmentation


class FeatureExtraction(pyPhasesFeatureExtraction):
    """augmentation for the physionet challenge 2023
    segmentSignal: Recordsignal of the segment with 18 EEG channels
    """

    def __init__(self, project) -> None:
        super().__init__()
        self.project = project

    def time(self, segmentSignal: RecordSignal):
        segmentLength = segmentSignal.getShape()[1]
        # segmentLengthInHOurs = segmentLength / segmentSignal.targetFrequency / 3600
        # hour, minutes = segmentSignal.start.split(":")
        # startTimeInHours = int(hour) + int(minutes) / 60
        # endTimeInHours = startTimeInHours + segmentLengthInHOurs
        hour = segmentSignal.start
        endTimeInHours = hour + 1
        return np.linspace(hour, endTimeInHours, segmentLength)

    def featureModel(self, segmentSignal: RecordSignal, featureName: str, xChannels: List[str]):
        import torch
        useGPU = torch.cuda.is_available() and False
        with self.project:
            config = self.project.config["featureConfigs", featureName]
            self.project.config.update(config)
            self.project.addConfig(config)

            self.project.trigger("configChanged", None)
            self.project.setConfig("trainingParameter.batchSize", 1)
            self.project.setConfig("recordWise", True)

            modelPath = self.project.getConfig("featureModel", torch)

            # this is needed for mutlithreading
            ModelManager.loadModel(self.project)
            # get feature model
            model = ModelManager.getModel(True)
            model.useGPU = useGPU
            state = model.load(modelPath)
            model.loadState(state)
            featureModel = model.model.eval()

            featureModel = featureModel.cuda() if useGPU else featureModel.cpu()

            # we assum that the segment is already preprocessed for the model
            array = segmentSignal.getSignalArray(xChannels, transpose=True)

            da = DataAugmentation(self.project.config, self.project.getConfig("datasetSplit"), recordAnnotations={})
            array, _ = da.augmentSegment(array, None)
            array = array.transpose(2, 1, 0)

            features = model.predict(array, get_likelihood=True)
            _, features = da.restoreLength(None, features, length=segmentSignal.getShape()[1])
            features = features[:, :, 1]

        return features
