from dataclasses import dataclass

import torch.nn as nn

from .Basics import Layer, LayerConfig, Reshape


@dataclass
class RNNLayerConfig(LayerConfig):
    hiddenSize: int = 128
    bidirectional: bool = False
    residual: bool = False
    type: str = "lstm"
    count = 1
    dropout = 0.0
    outputLayerType: str = "conv"
    out: int = 32

    def outputSize(self, inputlength=None, inputChannels=None):
        return 1, self.out


class RNNLayer(Layer):
    def buildLayer(self, length, config: RNNLayerConfig):
        return RNN(self.in_channels, length, config)

    def outputSize(self, inputlength=None, inputChannels=None):
        return self.config.outputSize(inputlength, inputChannels)


class RNN(nn.Module):
    def outputSize(self, inputlength=None, inputChannels=None):
        return self.config.outputSize(inputlength, inputChannels)

    def __init__(self, channel_in, length, config: RNNLayerConfig = None):
        super().__init__()

        self.config = config
        self.length = length
        self.channel_in = channel_in
        outputSize = config.out
        self.out = outputSize

        rnnInput = {
            "input_size": length,
            "hidden_size": config.hiddenSize,
            "num_layers": config.count,
            "batch_first": True,
            "dropout": config.dropout,
            "bidirectional": config.bidirectional,
        }

        if config.type == "lstm":
            self.rnn = nn.LSTM(**rnnInput)
        elif config.type == "rnn":
            self.rnn = nn.RNN(**rnnInput)
        elif config.type == "gru":
            self.rnn = nn.GRU(**rnnInput)
        else:
            raise Exception("rnn type not supported: %s" % config.type)

        self.hidden = None

        hiddenSize = config.hiddenSize

        self.reshape = False
        self.permute = False

        if config.bidirectional:
            hiddenSize *= 2

        if config.bidirectional:
            hiddenSize *= 2

        if config.outputLayerType == "conv":
            self.outputLayer = nn.Conv1d(
                in_channels=hiddenSize,
                out_channels=outputSize,
                groups=1,
                kernel_size=1,
                padding=0,
            )
            # if there are multiple channels, create a fina
            if self.channel_in > 1:
                sizeAfterConv = outputSize * self.channel_in
                self.outputLayer = nn.Sequential(
                    self.outputLayer,
                    Reshape(-1, sizeAfterConv),
                    nn.Linear(sizeAfterConv, outputSize),
                )

            self.permute = True
        elif config.outputLayerType == "full":
            self.outputLayer = nn.Linear(hiddenSize * self.channel_in, outputSize)
            self.reshape = (-1, hiddenSize * self.channel_in)
            # self.permute = True
        else:
            raise Exception("outputlayer type not supported: %s" % config.outputLayerType)

        if config.residual:
            raise Exception("residual not supported")

        # # Output convolution to map the LSTM hidden states from forward and backward pass to the output shape
        # self.outputConv1 = nn.Conv1d(in_channels=hiddenSize * 2, out_channels=hiddenSize, groups=1, kernel_size=1, padding=0)
        # self.outputConv2 = nn.Conv1d(in_channels=hiddenSize, out_channels=out_channels, groups=1, kernel_size=1, padding=0)

        # # Residual mapping
        # self.identMap1 = nn.Conv1d(in_channels=in_channels, out_channels=hiddenSize, groups=1, kernel_size=1, padding=0)

    def forward(self, x):
        # batchsize, channelcount, length
        batchSize = x.shape[0]
        y = x
        # y = x.view(batchSize, -1, 1)
        y, self.hidden = self.rnn(y)  # , self.hidden
        # y = x.view(batchSize, self.in_channels, -1)

        if self.reshape:
            y = y.reshape(self.reshape)

        if self.permute:
            y = y.permute(0, 2, 1)

        y = self.outputLayer(y)

        if not self.reshape:
            y = y.reshape(batchSize, self.out)

        return y
