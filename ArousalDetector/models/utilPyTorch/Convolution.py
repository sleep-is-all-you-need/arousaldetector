import math
from dataclasses import dataclass

import torch.nn as nn

from .Basics import Layer, LayerConfig


@dataclass
class ConvLayerConfig(LayerConfig):
    filter: int = 256
    kernel: int = 9
    stride: int = 1
    activation: str = None
    padding: bool = False
    bias: bool = True

    def calculateoutputLength(inputLength, kernel, stride=1):
        return math.floor((inputLength - kernel) / stride) + 1

    def outputSize(self, inputLength, inputChannels=None):
        if self.padding:
            return int(inputLength / self.stride), self.filter

        return (
            ConvLayerConfig.calculateoutputLength(inputLength, self.kernel, self.stride),
            self.filter,
        )


class ConvLayer(Layer):
    def buildLayer(self, length, config: ConvLayerConfig):
        if config.padding and config.stride > 1:
            padding = int((config.kernel - 1) // 2)
        else:
            padding = "same" if config.padding else 0
        return nn.Conv1d(
            in_channels=self.in_channels,
            out_channels=config.filter,
            kernel_size=config.kernel,
            stride=config.stride,
            padding=padding,
            bias=config.bias,
        )

    def outputSize(self, inputlength=None, inputChannels=None):
        return self.config.outputSize(inputlength)


class ConvLayersComposition(nn.Module):
    def buildLayer(self, out, length, config):
        return ConvLayer(out, length, config)


class ConvTransposeLayer(Layer):
    def buildLayer(self, config: ConvLayerConfig):
        return nn.ConvTranspose1d(
            in_channels=self.in_channels,
            out_channels=config.filter,
            kernel_size=config.kernel,
            stride=config.stride,
        )

    def outputSize(self, inputlength=None):
        return self.config.outputSize(inputlength)


class ConvTransposeLayersComposition(nn.Module):
    def buildLayer(self, out, config):
        return ConvTransposeLayer(out, config)
