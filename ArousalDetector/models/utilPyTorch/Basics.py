from dataclasses import dataclass
import math
from typing import List, OrderedDict
import torch.nn as nn
import inspect


class ModelConfig:
    @classmethod
    def fromDict(cls, dataDict: dict):
        return cls().update(dataDict)

    def update(self, dataDict: dict):
        cls = self.__class__
        signatureItems = inspect.signature(cls).parameters.keys()
        for key, value in dataDict.items():
            if key in signatureItems:
                curValue = self.__getattribute__(key)
                if isinstance(curValue, ModelConfig):
                    curValue.update(value)
                else:
                    self.__setattr__(key, value)
        return self

    def toDict(self):
        cls = self.__class__
        d = dict()
        for key in inspect.signature(cls).parameters.keys():
            value = self.__getattribute__(key)
            if isinstance(value, ModelConfig):
                d[key] = value.toDict()
            else:
                d[key] = value
        return d

    def copy(self):
        return type(self).fromDict(self.toDict())


class Reshape(nn.Module):
    def __init__(self, *shape):
        super().__init__()
        self.shape = shape

    def forward(self, x):
        return x.reshape(self.shape)


@dataclass
class LayerConfig(ModelConfig):
    activation: str = None
    batchNorm: bool = False
    batchNormBeforeActivation: bool = True
    parallel: bool = False
    buildedLayer = None
    overwriteInChannels: int = None


class Layer(nn.Module):
    def __init__(self, in_channels, config: LayerConfig, length):
        super().__init__()

        self.config = config
        self.in_channels = in_channels if config.overwriteInChannels is None else config.overwriteInChannels
        self.length = length

        self.layers = nn.Sequential()

        if config.parallel:
            self.layers.add_module("main", nn.DataParallel(self.buildLayer(length, config)))
        else:
            self.layers.add_module("main", self.buildLayer(length, config))

        if config.batchNorm and config.batchNormBeforeActivation:
            self.layers.add_module("batchNorm", nn.BatchNorm1d(self.outputSize(length, in_channels)[1]))

        if config.activation is not None:
            if config.activation == "relu":
                activation = nn.ReLU(inplace=True)
            elif config.activation == "sigmoid":
                activation = nn.Sigmoid()
            elif config.activation == "gelu":
                activation = nn.GELU()
            elif config.activation == "leakyrelu":
                activation = nn.LeakyReLU()
            elif config.activation == "tanh":
                activation = nn.Tanh()
            elif config.activation == "softmax":
                activation = nn.Softmax(dim=1)
            else:
                raise Exception("activation %s is not yet supported" % (config.activation))
            self.layers.add_module("activation", activation)

        if config.batchNorm and not config.batchNormBeforeActivation:
            self.layers.add_module("batchNorm", nn.BatchNorm1d(self.outputSize(0)[1]))

    def forward(self, x):
        return self.layers(x)

    def outputSize(self, inputlength=None, inputChannels=None):
        return self.config.outputSize(inputlength, inputChannels)


@dataclass
class LinearLayerConfig(LayerConfig):
    out: int = 256

    def outputSize(self, inputLength=None, inputChannels=None):
        return None, self.out


class LinearLayer(Layer):
    def buildLayer(self, length, config: LayerConfig):
        return nn.Linear(self.in_channels, config.out)

    def forward(self, x):
        # x = x.permute(0, 2, 1)
        return self.layers(x)


class FullLayer(Layer):
    def buildLayer(self, length, config: LinearLayerConfig):
        length = 1 if length is None else length
        return nn.Linear(self.in_channels * length, config.out)

    def forward(self, x):
        batchSize = x.shape[0]
        return self.layers(x.reshape(batchSize, -1))


@dataclass
class MaxPoolingLayerConfig(LayerConfig):
    size: int = 2
    kernel: int = 2

    def outputSize(self, inputLength, inputChannels=None):
        return math.floor((inputLength - self.kernel) / self.size) + 1, inputChannels


class MaxPoolingLayer(Layer):
    def buildLayer(self, length, config: MaxPoolingLayerConfig):
        return nn.MaxPool1d(stride=config.size, kernel_size=config.kernel)


class BatchnormalizationLayerConfig(LayerConfig):
    def outputSize(self, inputLength=None, inputChannels=None):
        return inputLength, inputChannels


class BatchnormalizationLayer(Layer):
    def buildLayer(self, length, config: BatchnormalizationLayerConfig):
        return nn.BatchNorm1d(self.in_channels)


@dataclass
class UpsamplingLayerConfig(LayerConfig):
    size: int = 2
    alignCorners: bool = False
    mode: str = "nearest"

    def outputSize(self, inputLength=None, inputChannels=None):
        return inputLength * self.size, inputChannels


class UpsamplingLayer(Layer):
    def buildLayer(self, length, config: UpsamplingLayerConfig):
        return nn.Upsample(
            scale_factor=config.size,
            mode=config.mode,
            align_corners=config.alignCorners,
        )


class Conv(Layer):
    def buildLayer(self, length, config: LinearLayerConfig):
        return nn.Linear(self.in_channels, config.out)

    def outputSize(self, inputLength=None, inputChannels=None):
        return inputLength, self.config.out


class ConvBlock(Layer):
    def buildLayer(self, length, config: LinearLayerConfig):
        return nn.Linear(self.in_channels, config.out)

    def outputSize(self, inputLength=None, inputChannels=None):
        return inputLength, self.config.out


@dataclass
class LayersCompositionConfig(ModelConfig):
    layers: List[LayerConfig] = None

    @staticmethod
    def fromListAndDefault(layerCounts, defaultConfig: LinearLayerConfig):
        config = LayersCompositionConfig()
        configs = []
        for count in layerCounts:
            c = defaultConfig.copy()
            c.out = count
            configs.append(c)
        config.layers = configs

        return config

    @staticmethod
    def fromArray(array):
        config = LayersCompositionConfig()
        config.layers = []

        for layerconfig in array:
            if isinstance(layerconfig, list):
                layer = LayersComposition
                baseConfig = LayersCompositionConfig.fromArray(layerconfig)
            else:
                type = layerconfig["type"]

                if type == "conv":
                    from .Convolution import ConvLayer, ConvLayerConfig

                    baseConfig = ConvLayerConfig.fromDict(layerconfig)
                    layer = ConvLayer
                elif type == "convTranspose":
                    from .Convolution import ConvTransposeLayer, ConvLayerConfig

                    baseConfig = ConvLayerConfig.fromDict(layerconfig)
                    layer = ConvTransposeLayer
                elif type == "maxpool":
                    baseConfig = MaxPoolingLayerConfig.fromDict(layerconfig)
                    layer = MaxPoolingLayer
                elif type == "batchnorm":
                    baseConfig = BatchnormalizationLayerConfig.fromDict(layerconfig)
                    layer = BatchnormalizationLayer
                elif type == "upsampling":
                    baseConfig = UpsamplingLayerConfig.fromDict(layerconfig)
                    layer = UpsamplingLayer
                elif type == "full":
                    baseConfig = LinearLayerConfig.fromDict(layerconfig)
                    layer = FullLayer
                elif type == "linear":
                    baseConfig = LinearLayerConfig.fromDict(layerconfig)
                    layer = LinearLayer
                elif type == "reshape":
                    baseConfig = ReshapeConfig.fromDict(layerconfig)
                    layer = ReshapeLayer
                elif type == "lstm":
                    from .RNN import RNNLayerConfig, RNNLayer

                    layerconfig["type"] = "lstm"
                    baseConfig = RNNLayerConfig.fromDict(layerconfig)
                    layer = RNNLayer
                else:
                    raise Exception("Layer type '%s' is not supported" % type)

            baseConfig.buildedLayer = layer
            config.layers.append(baseConfig)
        return config

    def outputSize(self, inputLength=None, inputChannels=None):
        for layer in self.layers:
            inputLength, inputChannels = layer.outputSize(inputLength, inputChannels)
        return inputLength, inputChannels


class LayersComposition(nn.Module):
    def __init__(self, inputsize, config: LayersCompositionConfig, length=None):
        super().__init__()

        self.config = config
        layers = []
        numOutputChannels = inputsize

        for i, config in enumerate(self.config.layers):
            if config.buildedLayer is None:
                layer = self.buildLayer(numOutputChannels, config, length)
            else:
                layer = config.buildedLayer(numOutputChannels, config, length)

            length, numOutputChannels = layer.outputSize(length, numOutputChannels)
            layers.append(("layer%i" % i, layer))

        layers = OrderedDict(layers)
        self.layers = nn.Sequential(layers)

    def forward(self, x):
        return self.layers(x)

    def buildLayer(self, out, length, config):
        return LinearLayer(out, length, config)

    def outputSize(self, inputlength, inputChannels=None):
        return self.config.outputSize(inputlength, inputChannels)


@dataclass
class ReshapeConfig(ModelConfig):
    shape: List[int] = None

    def outputSize(self, inputLength=None, inputChannels=None):
        return self.shape[1], self.shape[2]

    def buildLayer(self, length, config: ModelConfig):
        return Reshape(self.shape)

    def outputSize(self, inputlength=None, inputChannels=None):
        return self.shape[1], self.shape[2]


class ReshapeLayer(nn.Module):
    def __init__(self, in_channels, config: ReshapeConfig, length):
        super().__init__()
        self.shape = config.shape

    def forward(self, x):
        return x.reshape(self.shape)

    def outputSize(self, inputlength=None, inputChannels=None):
        return self.shape[1], self.shape[2]


class Reshape(nn.Module):
    def __init__(self, *shape):
        super().__init__()
        self.shape = shape

    def forward(self, x):
        return x.reshape(self.shape)
