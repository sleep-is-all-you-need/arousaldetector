from dataclasses import dataclass
from typing import Dict, List

import torch
from torch import nn

from pyPhasesML.adapter.ModelTorchAdapter import ModelTorchAdapter

from ..utilPyTorch.Basics import (
    LayerConfig,
    LayersComposition,
    LayersCompositionConfig,
    ModelConfig,
)


@dataclass
class UnetConfig(ModelConfig):
    conv: LayersCompositionConfig = None
    finalLayers: LayersCompositionConfig = None
    layers: List[Dict] = None
    defaultConvolution: LayerConfig = None
    defaultUpsample: LayerConfig = None
    useConvolutionTranspose: bool = False
    middleLayerAverage: bool = True
    middleLayerAverageDown: bool = False
    oneHotDecoded: bool = True
    useSoftMaxOnEval: bool = False


class UNet(nn.Module):
    """Build-up the DeepSleep 2.0 network from the previous modules"""

    saveX = []

    # def __init__(self, in_channels=13, linear=True, numClasses=2, smoothWindow=0, parallel=False):

    def __init__(self, in_channels, signalLength, numClasses, config: UnetConfig):
        super().__init__()

        finalConfig = LayersCompositionConfig.fromArray(config.conv)
        self.conv_layer = LayersComposition(in_channels, finalConfig, signalLength)

        self.in_channels = in_channels
        # oneHotDecoded = config.oneHotDecoded

        # build configs
        downConfig = LayersCompositionConfig()
        downConfig.layers = []
        upConfig = LayersCompositionConfig()
        upConfig.layers = []

        convLength, convChannels = self.conv_layer.outputSize(signalLength)

        downLayers = []
        upLayers = []

        self.saveX = []
        self.restoreX = []
        layerCount = sum([3 if layer[1] > 1 else 2 for layer in config.layers])
        lastSize = convChannels
        lastLastSize = convChannels

        lastReduction = 0
        for channels, reduction, kernelSize in config.layers:
            self.saveX.append(len(downLayers))
            # self.restoreX.append(layerCount - len(upLayers))
            if reduction > 1:
                downLayers.append(
                    {
                        "type": "maxpool",
                        "kernel": reduction,
                        "size": reduction,
                    }
                )

            convDict = config.defaultConvolution.copy()
            convDict["kernel"] = kernelSize
            convDict["filter"] = lastSize + (channels - lastSize) // 2 if config.middleLayerAverageDown else channels

            # double conv
            downLayers.append(convDict.copy())
            convDict["filter"] = channels
            downLayers.append(convDict)

            convDict = convDict.copy()
            # convDict["filter"] = lastSize // 2 if reduction > 1 else lastSize
            convDict["filter"] = lastLastSize
            upLayers.insert(0, convDict)

            convDict = convDict.copy()
            convDict["filter"] = (channels + lastSize) // 2 if config.middleLayerAverage else lastSize
            convDict["overwriteInChannels"] = lastSize + lastSize
            upLayers.insert(0, convDict)

            if reduction > 1:
                upsample = config.defaultUpsample.copy()
                upsample["size"] = reduction
                upLayers.insert(0, upsample)

            layerCount -= 3 if lastReduction > 1 else 2
            self.restoreX.append(layerCount)
            lastReduction = reduction

            lastLastSize = lastSize
            lastSize = channels

        self.saveX = torch.tensor(self.saveX)

        downConfig = LayersCompositionConfig.fromArray(downLayers)
        self.downLayers = LayersComposition(convChannels, downConfig, convLength)
        length, channels = self.downLayers.outputSize(signalLength)

        upConfig = LayersCompositionConfig.fromArray(upLayers)
        self.upLayers = LayersComposition(channels, upConfig, length)
        length, channels = self.upLayers.outputSize(length)
        
        # final layer
        finalConfig = LayersCompositionConfig.fromArray(config.finalLayers)
        finalConfig.layers[-1].filter = numClasses if config.oneHotDecoded else 1
        
        self.finalLayers = LayersComposition(channels, finalConfig, length)

        """Initialize the weights of all convolutional layers with Xavier uniform"""
        for m in self.modules():
            if type(m) == nn.Conv1d:
                nn.init.xavier_uniform_(m.weight, gain=nn.init.calculate_gain("relu"))

        self.useSoftMaxOnEval = config.useSoftMaxOnEval

    def forward(self, x):
        x = self.conv_layer(x)

        downX = []
        for index, layer in enumerate(self.downLayers.layers):
            # save inputs before every max pool ratio
            if index in self.saveX:
                downX.append(x)
            x = layer(x)

        for index, layer in enumerate(self.upLayers.layers):
            # concat before every double conv
            if index in self.restoreX:
                relX = downX.pop()
                x = torch.cat([relX, x], dim=1)

            x = layer(x)

        x = self.finalLayers(x)
        
        if self.useSoftMaxOnEval and not self.training:
            x = nn.Softmax(dim=1)(x)


        return x


class CustomBCELoss(nn.Module):
    def __init__(self):
        super(CustomBCELoss, self).__init__()
        self.loss = nn.BCELoss()

    def forward(self, y_hat, y):
        y_hat = y_hat.reshape(-1)
        y = y.view(-1)

        y_hat = y_hat[y > -0.5]
        y = y[y > -0.5]

        return self.loss(y_hat, y)


class UnsharpEventLoss(nn.Module):
    def __init__(self, padding=0):
        super().__init__()
        self.loss = nn.MultiLabelSoftMarginLoss(reduction="none")
        self.center = 0.5
        self.stdDev = 0.1
        self.padding = padding

    def normalDistribution(self, x, div=0.5, mean=5):
        div = torch.tensor(div)
        mean = torch.tensor(mean)
        normalDistri1 = 1 / torch.sqrt(2 * torch.pi * torch.square(div))
        normalDistri2 = -0.5 * torch.square((x - mean) / div)
        return normalDistri1 * torch.exp(normalDistri2)

    def forward(self, y_pred, y_truth):
        sampleLoss = self.loss(y_pred, y_truth.double())
        eventsAndCount = torch.unique_consecutive(y_truth[:, 0] == 0, return_counts=True)
        index = 0

        length = y_truth.shape[0]

        for isEvent, count in zip(*eventsAndCount):
            if isEvent:
                start = index - self.padding
                overlayLength = count + 2 * self.padding
                end = start + overlayLength

                if start < 0:
                    overlayLength += start
                    start = 0

                realLength = overlayLength - end + length if end > length else overlayLength

                dis = (
                    self.normalDistribution(
                        torch.arange(overlayLength, device=y_pred.device) / overlayLength,
                        self.stdDev,
                        self.center,
                    )
                    * 0.25
                    + 0.7
                )
                # disMoreOnEdge = (
                #     self.normalDistribution(
                #         torch.arange(overlayLength, device=y_pred.device) / overlayLength, self.stdDev, self.center
                #     )
                #     * 0.25
                # )

                # for i in range(classCount):
                #     # realLength = sampleLoss[start : start + overlayLength, i]
                predictedPositive = y_pred[start:end, 1] > 0.5
                predictedNegative = y_pred[start:end, 1] < 0.5
                positive = y_truth[start:end, 1] == 1
                negative = y_truth[start:end, 1] == 0
                # sampleLoss[start:end] *=

                # falsePositive = sampleLoss[y_pred[start:end, 1] > 0.5] and y_truth[start:end, 1] == 1
                sampleLoss[start:end][predictedNegative & positive] *= (
                    1 - dis[0:realLength][predictedNegative & positive] + 1.25
                )
                sampleLoss[start:end][predictedPositive & negative] *= dis[0:realLength][predictedPositive & negative]

            index += count

        return sampleLoss.mean()


class EventLossWrap(nn.Module):
    def __init__(self, loss, normalize=False, countLossWeight=1):
        super().__init__()
        self.loss = loss
        self.normalize = normalize
        # self.countLoss = torch.nn.MSELoss()
        self.smothLoss = torch.nn.SmoothL1Loss()
        self.countLossWeight = countLossWeight

    def forward(self, y_pred, y_truth):
        loss = self.loss(y_pred, y_truth)

        eventsAndCountTrue = torch.unique_consecutive(y_truth[:, 0] == 0, return_counts=True)
        eventsAndCountPred = torch.unique_consecutive(y_pred[:, 0] > y_pred[:, 1], return_counts=True)

        countTrue = torch.sum(eventsAndCountTrue[0], dtype=torch.float32)
        countPred = torch.sum(eventsAndCountPred[0], dtype=torch.float32)

        countLoss = (countPred - countTrue).abs() / countTrue

        self.stats = {
            "eventCountTruth": int(countTrue),
            "eventCountPredicted": int(countPred),
            "countLoss": float(countLoss),
        }

        return loss + countLoss * self.countLossWeight


class CustomBCEWithLogitsLoss(nn.Module):
    def __init__(self):
        super().__init__()
        self.loss = nn.BCEWithLogitsLoss()

    def forward(self, y_hat, y):
        y_hat = y_hat.view(-1)
        y = y.view(-1)

        y_hat = y_hat[y > -0.5]
        y = y[y > -0.5]

        return self.loss(y_hat, y.double())


class CategoricalCrossEntropyLoss(nn.Module):
    def __init__(self, weight=None):
        super().__init__()
        self.loss = nn.CrossEntropyLoss(weight=weight)

    def forward(self, y_hat, y):
        return self.loss(y_hat, y.argmax(dim=1))

class FocalLoss(nn.Module):
    def __init__(self, weight=None, gamma=2):
        super(FocalLoss, self).__init__()
        self.gamma = gamma
        self.weight = weight

    def forward(self, y_hat, y):
        ce_loss = nn.functional.cross_entropy(y_hat, y, weight=self.weight)
        pt = torch.exp(-ce_loss)
        focal_loss = ((1 - pt) ** self.gamma * ce_loss).mean()
        return focal_loss



class ConfigUnet(ModelTorchAdapter):
    def initialOptions(self):
        return {
            # "encodingChannels": [15, 18, 21, 25, 30, 60, 120, 240, 480],
            # "decodingChannels": [480, 240, 120, 60, 30, 25, 21, 18, 15],
            # "poolingSizes": [2, 4, 4, 4, 4, 4, 4, 4, 4],
            # "parallel": False,
            # "linear": True,
            # "smoothWindow": 0,
            # "loss": {"type": None, "countLossWeight": 0},
            # "normalization": None,
        }

    def define(self, normaliziation=None):
        inputShape = self.inputShape
        numClasses = self.config.numClasses
        self.oneHotDecoded = self.getOption("oneHotDecoded")
        self.lossOption = self.getOption("loss")

        config = UnetConfig.fromDict(self.options)

        # norm = self.getOption("normalization")
        # self.skipNormalize = False
        # if norm is not None and norm is not True and "mean" in norm:
        #     normaliziation = norm
        #     self.skipNormalize = True

        # self.lossOptions = self.getOption("loss")

        self.model = UNet(inputShape[1], inputShape[0], numClasses, config)
        classWeights = self.getOption("classWeights") if "classWeights" in self.options else None
        self.weightTensors = None if classWeights is None else torch.IntTensor(classWeights)


        if self.useGPU:
            self.weightTensors = None if self.weightTensors is None else self.weightTensors.cuda()
            self.model.cuda()
            
        self.lossDimension = self.config.numClasses if self.oneHotDecoded else 1

    def getLossFunction(self):
        if self.lossOption == "CrossEntropy":
            return nn.CrossEntropyLoss()
        elif self.lossOption == "CustomBCEWithLogitsLoss":
            return CustomBCEWithLogitsLoss()
        elif self.lossOption == "CategoricalCrossEntropyLoss":
            return CategoricalCrossEntropyLoss(self.weightTensors)
        elif self.lossOption == "Focal":
            return FocalLoss(weight=self.weightTensors)
        else:
            return CustomBCELoss()
        # return CustomBCEWithLogitsLoss()

        # defaultLoss = CategoricalCrossEntropyLoss() if self.config.numClasses > 2 else CustomBCEWithLogitsLoss()
        # type = self.lossOptions["type"]
        # countLossWeight = self.lossOptions["countLossWeight"]

        # if type is None:
        #     return defaultLoss

        # if type == "EventErrorLoss":
        #     if countLossWeight == 0:
        #         raise Exception("please specify countLossWeight")

        #     return EventLossWrap(defaultLoss, countLossWeight=countLossWeight)

        # return defaultLoss

    def mapOutput(self, output):
        return output.transpose(2, 1)

    def mapOutputForLoss(self, output, mask=None):
        output = self.mapOutput(output)
        output = output.reshape(-1, self.lossDimension)
        # output = output.contiguous().reshape(-1, self.config.numClasses)
        if mask is not None:
            output = output[mask.reshape(-1)]

        return output

    def mapOutputForPrediction(self, output):
        return self.mapOutput(output)

    # def beforeTrain(self, dataset):
    #     if self.getOption("normalization") and self.skipNormalize == False:
    #         param = NormalizationModule.calculateNormalizationParameter(dataset)
    #         self.define(param)
