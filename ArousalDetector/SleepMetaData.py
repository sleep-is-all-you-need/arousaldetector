from pyPhases.util.Logger import classLogger

from ArousalDetector.PSGEventManager import PSGEventManager


@classLogger
class SleepMetaData:
    validationNameIdMap = {}

    def __init__(self):
        self.metaDataMap = {}
        self.metaData = []

    def getEventsFromSignal(self, signal, classification):
        eventmanager = PSGEventManager()
        return [e for e in eventmanager.getEventsFromSignal(signal, classification) if e.name != "None"]


    def getIndexForEventCount(self, eventCount):
        tst = self.getMetaData("tst", 0)

        if tst == 0:
            return None

        return eventCount / tst * 60

    def addCountAndIndex(self, name, eventCount):
        self.registerMetadata(f"count{name}", eventCount, unit="#")
        self.registerMetadata(f"index{name}", self.getIndexForEventCount(eventCount), unit="#")
    
    def fromApneaSignal(self, signal, classification=None):
        classification = classification or ["None", "obstructive", "mixed", "central", "hypopnea"]
        events = self.getEventsFromSignal(signal, classification)

        eventCount = len(events)
        self.registerMetadata("countApneaHypopnea", eventCount, unit="#")
        self.registerMetadata("ahi", self.getIndexForEventCount(eventCount))

    def fromArousalSignal(self, signal, classification=None):
        classification = classification or ["None", "Arousal"]
        events = self.getEventsFromSignal(signal, classification)

        self.addCountAndIndex("Arousal", len(events))

    def registerMetadata(self, name, value, unit="s"):
        # normalize to minutes, because of alice
        if unit == "s" and value is not None:
            value /= 60

        if name in self.metaDataMap:
            raise Exception("matadata %s allready exist!" % name)

        metaData = {
            "name": name,
            "value": value,
            "unit": unit,
        }

        self.metaDataMap[name] = metaData
        self.metaData.append(metaData)

    def getMetaData(self, name, fallBack=None, unit="s"):

        if name not in self.metaDataMap:
            return fallBack

        data = self.metaDataMap[name]
        value = data["value"]
        if data["unit"] == "s":
            value *= 60

        return value
    