import numpy as np
import scipy
from pyPhases import Phase
from pyPhasesML import Model, ModelManager
from tqdm import tqdm

from ArousalDetector.DataAugmentation import DataAugmentation
from ArousalDetector.EventScorer import EventScorer


class ThresholdOptimisation(Phase):
    """
    calculate the optimal threshold for the given metric
    """

    allowTraining = False

    def getModel(self) -> Model:
        modelState = self.project.getData("modelState", Model, generate=self.allowTraining)
        model = ModelManager.getModel(True)
        model.build()
        model.loadState(modelState)
        return model

    def calculateValidationData(self):
        model = self.getModel()
        useTrainValForOptimisation = self.getConfig("optimizeOn")
        assert useTrainValForOptimisation in ["validation", "training", "trainval"]
        validationData = self.project.generateData(f"dataset-{useTrainValForOptimisation}")

        prediction = []
        truth = []
        for x, t in tqdm(validationData):
            p = model.predict(x)
            prediction.append(p)
            truth.append(t)

        prediction = np.concatenate(prediction, axis=0).reshape(-1)
        truth = np.concatenate(truth).reshape(-1)

        return prediction, truth

    def calculateThreshold(self):
        thresholdMetric = self.getConfig("thresholdMetric", None)

        # get Validation data
        # optimize threshold for given metric
        scorer = EventScorer(classNames=self.getConfig("classification.classNames"), trace=True)
        scorer.metrics = [thresholdMetric]
        augmentAfterPredict = self.getConfig("eventEval.dataAugmentationAfterPredict", False)
        da = DataAugmentation(self.project.config, "test", threshold=scorer.threshold)

        prediction, truth = self.getData("validationResult", tuple)

        biggerIsBetter = scorer.getMetricDefinition(thresholdMetric)[2]

        def score(threshold, p, t):
            if threshold >= 1 or threshold <= 0:
                return np.inf

            negate = -1 if biggerIsBetter else 1
            da.threshold = threshold[0]
            p, t = da.augmentBatchesByConfig(p.copy().reshape(1, -1, 1), t.copy().reshape(1, -1, 1), augmentAfterPredict)
            scorer.threshold = threshold[0]
            r = scorer.score(t, p)

            self.log(f"Optimizing threshold by {thresholdMetric}: t = {threshold} => {r[thresholdMetric]} ")

            return r[thresholdMetric] * negate

        threshold = scipy.optimize.fmin(score, args=(prediction, truth), x0=0.3, xtol=0.001, ftol=0.001)

        self.logSuccess(f"Optimized threshold for {thresholdMetric}: {threshold}")

        return threshold

    def generateData(self, name):
        if name == "validationResult":
            valData = self.calculateValidationData()
            self.registerData("validationResult", valData)
        if name == "threshold":
            numClasses = self.getConfig("numClasses")
            thresholdMetric = self.getConfig("thresholdMetric", None)

            if numClasses == 2 and thresholdMetric is not None:
                threshold = self.calculateThreshold()
            else:
                threshold = 0.5
            self.registerData("threshold", threshold)

    def main(self):
        self.getData("threshold", float)
