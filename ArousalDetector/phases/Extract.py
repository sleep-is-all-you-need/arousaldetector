import numpy as np
from pyPhases import Phase
from pyPhases.util import BatchProgress
from pyPhasesML import DataversionManager
from pyPhasesRecordloader import AnnotationNotFound, ChannelsNotPresent, RecordLoader

from ArousalDetector.PSGEventManager import PSGEventManager
from ArousalDetector.SignalPreprocessing import SignalPreprocessing
from ArousalDetector.FeatureExtraction import FeatureExtraction


class RecordProcessor:
    """this class is needed to tailor the multithreading"""

    def __init__(
        self,
        recordLoader,
        preProcessingConfig,
        signalProcessing,
        eventManager,
        labelChannels,
        classificationName,
        project=None,
    ) -> None:
        self.recordLoader = recordLoader
        self.preProcessingConfig = preProcessingConfig
        self.signalProcessing = signalProcessing
        self.eventManager = eventManager
        self.labelChannels = labelChannels
        self.classificationName = classificationName
        self.channelNotPresent = []
        self.project = project

    def prepareLabelSignal(self, name, eventSignal, signalLength):
        """
        create the Y signal that is used in training, from the event signal that is loaded from the record
        """
        if self.classificationName not in eventSignal:
            raise ChannelsNotPresent(self.classificationName)

        labelSignalArray = np.zeros(signalLength)
        if name == "SleepArousals":
            eventSignal = eventSignal["arousal"]
            labelSignalArray[eventSignal > 1] = 1
        elif name == "SleepStagesAASM":
            eventSignal = eventSignal["sleepStage"]
            labelSignalArray[eventSignal == PSGEventManager.INDEX_WAKE] = 0
            labelSignalArray[eventSignal == PSGEventManager.INDEX_REM] = 1
            labelSignalArray[eventSignal == PSGEventManager.INDEX_NREM1] = 2
            labelSignalArray[eventSignal == PSGEventManager.INDEX_NREM2] = 3
            labelSignalArray[eventSignal == PSGEventManager.INDEX_NREM3] = 4
        elif name == "SleepApnea":
            eventSignal = eventSignal["apnea"]
            labelSignalArray[:] = 0
            labelSignalArray[eventSignal == PSGEventManager.INDEX_APNEA_OBSTRUCTIVE] = 1
            labelSignalArray[eventSignal == PSGEventManager.INDEX_APNEA_MIXED] = 2
            labelSignalArray[eventSignal == PSGEventManager.INDEX_APNEA_CENTRAL] = 3
            labelSignalArray[eventSignal == PSGEventManager.INDEX_APNEA_HYPO] = 4
        else:
            raise Exception("unknown label channel: " + name)
        return labelSignalArray

    def preparelabelChannels(self, eventSignal, signalLength):
        labelChannels = []
        for name in self.labelChannels:
            signal = self.prepareLabelSignal(name, eventSignal, signalLength)
            signal = signal.reshape(-1, 1)
            labelChannels.append(signal)

        return np.concatenate(labelChannels, axis=1)

    @staticmethod
    def tailorToSleepScoring(recordSignal, events, useSPT=False, fillLastSleepstage=False):
        """
        some strategies to tailor the record to the sleep or light annotations
        """
        sleepEventNames = ["R", "N1", "N2", "N3"]
        offsetStart = None
        offsetEnd = None

        if not useSPT:
            sleepEventNames.append("W")
            ligthEvents = [e for e in events if e.name in ["lightOn", "lightOff"]]
            if lightOff := [e for e in ligthEvents if e.name == "lightOff"]:
                offsetStart = lightOff[0].start

            if lightOn := [e for e in ligthEvents if e.name == "lightOn"]:
                offsetEnd = lightOn[0].start

        sleepEvents = [e for e in events if e.name in sleepEventNames]

        if not sleepEvents:
            raise ChannelsNotPresent("SleepStagesAASM")

        startOffset = sleepEvents[0].start if offsetStart is None else offsetStart
        if sleepEvents[-1].duration == 0:
            tarFreq = recordSignal.targetFrequency
            sleepEvents[-1].duration = len(recordSignal) / tarFreq - sleepEvents[-1].start if fillLastSleepstage else 30
        endOffset = sleepEvents[-1].end() if offsetEnd is None else offsetEnd

        if startOffset == 0 and endOffset is None:
            return events

        recordSignal.signalOffset(startOffset, endOffset)

        newEvents = []
        for event in events:
            if startOffset > 0:
                if event.end() <= startOffset:
                    continue
                if event.start < startOffset:
                    event.duration -= startOffset - event.start
                    event.start = 0
                else:
                    event.start -= startOffset
            if endOffset is not None:
                if event.start >= endOffset - startOffset:
                    continue
                if event.end() > endOffset - startOffset:
                    event.duration -= event.end() - endOffset + startOffset
            newEvents.append(event)

        if not event:
            raise ChannelsNotPresent("Arousals")

        return newEvents

    def __call__(self, recordId):
        """ 
        create the X and Y signal from the record signals and event list
        """
        try:
            recordSignal, events = self.recordLoader.loadRecord(recordId)

            targetSignals = self.preProcessingConfig["targetChannels"]
            targetFrequency = self.preProcessingConfig["targetFrequency"]
            labelFrequency = self.preProcessingConfig["labelFrequency"]
            featureChannels = self.preProcessingConfig["featureChannels"]
            recordSignal.targetFrequency = targetFrequency

            # set the target frequency for the signal to get the correct signal length
            self.signalProcessing.preprocessingSignal(recordSignal)

            events = RecordProcessor.tailorToSleepScoring(
                recordSignal,
                events,
                useSPT=self.preProcessingConfig["cutFirstAndLastWake"],
                fillLastSleepstage=self.preProcessingConfig["fillLastSleepstage"],
            )
            if "extendEvents" in self.preProcessingConfig:
                events = self.signalProcessing.extendEvents(events, self.preProcessingConfig["extendEvents"])

            FeatureExtraction(self.project).extractChannelsByConfig(recordSignal, featureChannels)

            signalLength = round(recordSignal.getSignalLength() * labelFrequency / targetFrequency)
            eventSignal = self.eventManager.getEventSignalFromList(
                events,
                signalLength,
                targetFrequency=labelFrequency,
                forceGapBetweenEvents=False,
            )
            # signalLength * 30
            eventSignal = self.preparelabelChannels(eventSignal, signalLength)
            processedSignal = recordSignal.getSignalArray(targetSignals)
        except ChannelsNotPresent as e:
            self.channelNotPresent.append(e.channels)
            return None
        except AnnotationNotFound as e:
            self.channelNotPresent.append(e.name)
            return None

        return processedSignal, eventSignal


class Extract(Phase):
    """
    Extract numpy arrays from the raw records in the dataset
    """

    useMultiThreading = True
    threads = None
    validatDataset = True

    def buildFromRecords(self, records, force=False):
        """
        extract the data from the records and save into preprocessing files
        """
        preprocessingConfig = self.getConfig("preprocessing")
        processRecord = RecordProcessor(
            recordLoader=RecordLoader.get(),
            preProcessingConfig=preprocessingConfig,
            signalProcessing=SignalPreprocessing(preprocessingConfig),
            eventManager=PSGEventManager(),
            labelChannels=self.getConfig("labelChannels"),
            classificationName=self.getConfig("classification.name"),
            project=self.project,
        )
        exporterSignals, dataIdSignals = self.project.getExporterAndId("data-processed", np.memmap)
        exporterFeatures, dataIdFeatures = self.project.getExporterAndId("data-features", np.memmap)
        exporterSignals.options["dtype"] = self.getConfig("preprocessing.dtype", "float32")
        exporterFeatures.options["dtype"] = self.getConfig("preprocessing.dtype", "float32")

        if force is False:
            if exporterSignals.exists(dataIdSignals) and exporterFeatures.exists(dataIdFeatures):
                self.logSuccess("Datafiles allready exist")
                return

            if exporterSignals.existsTmp(dataIdSignals) and exporterFeatures.existsTmp(dataIdFeatures):
                exporterSignals.loadTmp(dataIdSignals)
                exporterFeatures.loadTmp(dataIdFeatures)
                recordsLoaded = len(exporterFeatures.currentArrayShapes[dataIdSignals][0])
                records = records[recordsLoaded:]
                self.logSuccess("Resuming extraction: %i records allready loaded / %i to go" % (recordsLoaded, len(records)))

        bp = BatchProgress(records)
        bp.useMultiThreading = self.useMultiThreading
        if self.threads is not None:
            bp.threads = self.threads

        removedRecordIndexes = []

        def combine(recordList, dataIndex):
            recordList = recordList.copy()
            # append the the data and the shape to a tmp file

            if None in recordList:
                rm = [i + dataIndex for i, r in enumerate(recordList) if r is None]
                [removedRecordIndexes.append(r) for r in rm]
                recordList = [r for r in recordList if r is not None]
                self.logWarning("Skipped %i records: channels/annotation not present" % len(rm))

            if len(recordList) > 0:
                signals, features = zip(*recordList)
                exporterSignals.saveAndAppendArray(dataIdSignals, signals)
                exporterFeatures.saveAndAppendArray(dataIdFeatures, features)

        bp.start(processRecord, afterBatch=combine)

        # save the tmp files to the final files
        exporterSignals.finishStream(dataIdSignals)
        exporterFeatures.finishStream(dataIdFeatures)

        return removedRecordIndexes

    def getDataVersionManager(self):
        """
        the dataversionmanager is used to split the data into training, validation and test set and also handles folds
        """
        groupedRecords = self.project.getData("allDBRecordIds", list)
        seed = self.getConfig("dataversion.seed", None)
        splits = self.getConfig("dataversion.split")
        datasetNames = self.getConfig("datasetSplits")

        dm = DataversionManager(groupedRecords, splits, seed)
        dm.validatDatasetVersion(raiseException=self.validatDataset)

        # check if there are removed records, this can only happen after the extraction
        # dataversionamnager has diffent records before and after extraction!
        with self.project:
            for datasetName in datasetNames:
                self.setConfig("datasetSplit", datasetName)
                if self.project.dataExistIn("removedRecordIndexes", list):
                    removedIndexes = self.getData("removedRecordIndexes", list, generate=False)
                    self.log("Removing incomplete records from dataversionmanager")
                    dm.removeRecordIndexesFromSplit(datasetName, removedIndexes)

        return dm

    def generateData(self, name):
        datasetName = self.getConfig("datasetSplit")
        if name == "dataversionmanager":
            dm = self.getDataVersionManager()
            self.registerData("dataversionmanager", dm, save=False)
        elif name[:5] == "data-":
            dm = self.getData("dataversionmanager")

            # map trainning/validation to trainval split
            batchName = datasetName
            if "trainval" in dm.splits and datasetName in ["training", "validation"]:
                batchName = "trainval"
            groupedRecords = dm.getRecordsForSplit(batchName)

            # extract the data
            removedRecordIndexes = self.buildFromRecords(groupedRecords)

            signals = self.getData("data-processed", np.memmap, generate=False)
            features = self.getData("data-features", np.memmap, generate=False)

            self.registerData("data-processed", signals)
            self.registerData("data-features", features)
            self.registerData("removedRecordIndexes", removedRecordIndexes)
            dm.removeRecordIndexesFromSplit(batchName, removedRecordIndexes)

    def main(self):
        recordLoader = RecordLoader.get()
        recordLoader.setupRemoteReadOrDownload()

        extractDatasets = self.getConfig("datasetSplits")

        for datasetName in extractDatasets:
            self.project.setConfig("datasetSplit", datasetName)
            self.generateData("data-processed")
