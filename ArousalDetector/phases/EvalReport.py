from pathlib import Path
import shutil
from matplotlib import axes

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import statsmodels.stats.multicomp as mc
from ArousalDetector.Reporter import Reporter

from pyPhases import Phase
from pyPhasesRecordloader import RecordLoader, RecordSignal, Signal
from scipy import stats
from scipy.stats import f, f_oneway

from ArousalDetector.Plot import Plot
from ArousalDetector.DataAugmentation import DataAugmentation
import yaml


class EvalReport(Phase):
    allowTraining: bool = True
    exampleSkipClass = ["0-0"]
    examplePaddingInS = 10
    highlight = None
    exampleSecondsPerInch = 2

    def getExternalTestset(self):
        with self.project:
            evalConfig = self.getConfig("evalOn")

            # try not to update dataversion, rather replace it completly
            if "dataversion" in evalConfig:
                self.project.config["dataversion"] = {}

            self.project.config.update(evalConfig)
            self.project.trigger("configChanged", None)
            split = self.getConfig("datasetSplit")
            dataset = self.project.getData(f"dataset-{split}", list)
            dm = self.getData("dataversionmanager")
            recordsMap = dm.getRecordsForSplit(split)

        return dataset, dm, recordsMap

    def createLogFolder(self, path):
        Path(path).mkdir(parents=True, exist_ok=True)
        Path(path).joinpath("/examples").mkdir(parents=True, exist_ok=True)

    def addAsset(self, name, extension="png", bbox_inches="tight", dpi=300):
        fullName = name + "." + extension
        # assetPath = Reporter.assetPath + "/" + fullName
        fullPath = self.evalPath + fullName
        self.createLogFolder(Path(fullPath).parent)
        plt.savefig(fullPath, bbox_inches=bbox_inches, dpi=dpi)
        plt.close()

    def plotRecordSignal(
        self, classExamples, recordSignal: RecordSignal, fileName, sliceValues=None, highlights=None, offset=0
    ):
        classNames = self.getConfig("classification.classNames")
        classificationNames = {}
        for i, name1 in enumerate(classNames):
            for j, name2 in enumerate(classNames):
                classificationNames[f"{i}-{j}"] = f"{name1} as {name2}"

        classificationName = self.getConfig("classification.name")
        labelFrequency = self.getConfig("preprocessing.labelFrequency")
        samplingrate = recordSignal.targetFrequency

        for classIndex, examples in classExamples.items():
            for eIndex, sliceValues in enumerate(examples):
                className = classificationNames[classIndex]
                if classIndex in self.exampleSkipClass:
                    continue
                paddingInS = self.examplePaddingInS
                padding = paddingInS * samplingrate
                highlights = []

                sliceValues = sliceValues[0] + offset, sliceValues[1] + offset

                sliceValues = sliceValues[0], sliceValues[0] + 50 * 100

                if self.highlight is not None:
                    s, e = self.highlight
                    s += paddingInS
                    e = e + paddingInS if e > 0 else e - paddingInS
                    highlights.append((s, e))

                self.plot.plotRecordSignal(
                    recordSignal,
                    sliceValues,
                    resample=False,
                    padding=padding,
                    secondsPerInch=self.exampleSecondsPerInch,
                    highlights=highlights,
                    title="Example for %s/%s in record '{record}' from {from}" % (classificationName, className),
                )
                plt.axhline(y=self.threshold, color="red")
                plt.text(1, self.threshold + 0.02, "Threshold", color="red")
                self.plot.save(f"examples/{eIndex}-{fileName}-{className}")

    def plotExamples(self, recordStats, segmentStats, name=""):
        channelNames = self.getConfig("evalChannelNames")

        exampleCount = 3
        # pick random records
        randomRecordIndexes = np.random.choice(len(recordStats), exampleCount, replace=False)

        testData, dm, recordsMap = self.getExternalTestset()
        recordIds = dm.getRecordsForSplit("test")

        self.setConfig("BuildDataset.useMultiThreading", False)
        da = DataAugmentation(self.project.config, "test")

        for i, recordIndex in enumerate(randomRecordIndexes):
            # create recordsignal for processed data
            samplingrate = self.getConfig("preprocessing.targetFrequency")
            X, Y = testData[recordIndex]
            classExamples = recordStats[recordIndex]["examples"]
            prediction = segmentStats[recordIndex]["prediction"]

            recordSignal = RecordSignal.fromArray(
                X, sourceFrequency=samplingrate, targetFrequency=samplingrate, names=channelNames, transpose=True
            )

            # add labels
            labelSignal = Signal("Arousal", Y[:, 0], samplingrate)
            # labelSignal = Signal(classificationName, Y[:, 0], samplingrate)
            recordSignal.addSignal(labelSignal)
            recordSignal.recordId = recordIds[recordIndex]

            # add prediction
            offset = (len(Y[:, 0]) - prediction.shape[0]) // 2
            prediction, _ = da._fixeSizeSingleChannel(prediction.reshape(1, -1, 1), 2**21, fillValue=0, position="center")
            predictionSignal = Signal("Prediction", prediction.reshape(-1), samplingrate)
            recordSignal.addSignal(predictionSignal)

            self.plotRecordSignal(classExamples, recordSignal, f"example-{i}-processed", offset=offset)

    def main(self):
        threshold = self.getConfig("fixedThreshold", False)
        self.threshold = threshold or self.getData("threshold", float)

        modelConfigString = self.project.getDataFromName("eventResults").getTagString()
        evalPath = f"eval/{modelConfigString}/"

        self.evalPath = evalPath
        self.reporter = Reporter("EvalReport")
        self.plot = Plot(evalPath)
        self.metrics = self.getConfig("trainingParameter.validationMetrics")

        self.reporter.createFolder()

        # self.copyRawData()
        # self.trainingPlot(evalPath + "training.log")
        # self.modelInfo()

        # self.log("save report to: %s" % self.reporter.getFilePath())
        # self.reporter.save()

        segmentResultsRecords, segmentResults = self.getData("evalResults", list)
        df = pd.DataFrame(segmentResultsRecords).transpose()

        # drop generated columns if they exist
        drop_colums = ["truth", "prediction", "all_values", "pos_values", "ign_values", "neg_values"]
        for col in drop_colums:
            if col in df.columns:
                df = df.drop(columns=[col])

        df.to_csv(f"{evalPath}recordResultsSegment.csv", index=False)

        # segmentResultsRecords, segmentResults = self.getData("evalResults")
        self.logSuccess(f"result segments: {segmentResults}")
        # resultRows, result = self.getData("eventResults")
        resultRows, result = self.getData("eventResults", list)
        self.logSuccess(f"result events: {result}")
        df = pd.DataFrame(resultRows)

        results = {
            "auprc": segmentResults["auprc"],
            "f1-segment": segmentResults["f1"],
            "f1": result["f1"],
            "acc-segment": segmentResults["accuracy"],
            "acc": result["accuracy"],
            "kappa-segment": segmentResults["kappa"],
            "kappa-event": result["kappa"],
            "eventCountDiff": result["eventCountDiff"],
        }

        # self.plotExamples(resultRows, segmentResultsRecords)

        relMetaData = self.getConfig("eval.clinicalMetrics")

        for m in relMetaData:
            df[f"{m}-prediction-diff"] = df[f"{m}-prediction"] - df[f"{m}-truth"]
            df[f"{m}-prediction-error"] = df[f"{m}-prediction-diff"].abs()

        df.to_csv(f"{evalPath}recordResultsEvents.csv", index=False)

        self.logSuccess(f"Results: {results}")
        results = {k: float(v) for k, v in results.items()}
        with open(f"{evalPath}results.yml", "w") as file:
            yaml.dump(results, file)

        self.project.saveConfig(f"{evalPath}project.config")
        self.log(f"Eval finished: {evalPath}")

    def copyRawData(self):
        modelConfigString = self.project.getDataFromName("modelState").getTagString()
        trainingLogPath = f"logs/{modelConfigString}/"
        shutil.copyfile(trainingLogPath + "log.csv", Reporter.basePath + "training.log")
        shutil.copyfile(trainingLogPath + "project.config", Reporter.basePath + "project.json")

    def trainingPlot(self, logFile):
        df = pd.read_csv(logFile)
        plt.plot(df["epoch"], df["loss"], label="Training Loss")
        plt.plot(df["epoch"], df[self.metrics[0]], label=self.metrics[0])
        self.reporter.addAsset("trainingsProcess")

    def modelInfo(self):
        model = self.project.getPhase("Eval").getModel()
        sum = model.summary()
        self.reporter.addVar("modelName", self.getConfig("modelName"))
        self.reporter.addVar("parameter", model.parameter, "humannumber")
        self.reporter.addVar("modelSummary", sum)
