from pathlib import Path
from pyPhases import Phase

from pyPhasesRecordloader import RecordLoader


class Setup(Phase):
    """
    prepare the configuration for the project
    """

    def prepareConfig(self):
        numClasses = len(self.getConfig("classification.classNames"))
        self.setConfig("numClasses", numClasses)
        defaultValidationAugmentation = self.getConfig("segmentAugmentationEval", self.getConfig("segmentAugmentation"))
        self.setConfig("segmentAugmentationEval", defaultValidationAugmentation)

        # alice loader
        RecordLoader.registerRecordLoader("RecordLoaderTest", "ArousalDetector.recordloaders")
        RecordLoader.registerRecordLoader("RecordLoaderTSM", "ArousalDetector.recordloaders")
        RecordLoader.registerRecordLoader("RecordLoaderDomino", "ArousalDetector.recordloaders")
        tsmPath = Path(self.getConfig("tsm-path"))
        self.project.setConfig("loader.tsm.filePath", tsmPath.as_posix())
        self.project.setConfig("loader.tsm.dataset.downloader.basePath", tsmPath.as_posix())

        tsmPath = Path(self.getConfig("tsm-domino-path"))
        self.project.setConfig("loader.tsm-domino.filePath", tsmPath.as_posix())
        self.project.setConfig("loader.tsm-domino.dataset.downloader.basePath", tsmPath.as_posix())

        # default fold is 0 if none is set
        self.setConfig("fold", self.getConfig("fold", 0))
        self.setConfig("datasetSplits", list(self.getConfig("dataversion.split", {}).keys()))
        self.setConfig("hasFolds", self.getConfig("dataversion.folds", 0) > 0)
