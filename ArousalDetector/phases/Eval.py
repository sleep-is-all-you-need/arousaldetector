import numpy as np
import pandas as pd
from pyPhases import Phase
from pyPhasesML import Model, ModelManager, Scorer
from tqdm import tqdm

from ArousalDetector.DataAugmentation import DataAugmentation
from ArousalDetector.EventScorer import EventScorer
from ArousalDetector.SleepMetaData import SleepMetaData


class Eval(Phase):
    allowTraining: bool = False
    exampleCount: int = 3

    def getModel(self) -> Model:
        modelState = self.project.getData("modelState", Model, generate=self.allowTraining)
        model = ModelManager.getModel(True)
        model.build()
        model.loadState(modelState)
        return model

    def getTestMetadata(self):
        """
        get the metadata for the testset like total sleep time
        """
        if not bool(self.getConfig("evalOn", {})):
            return self.getData("metadata", list)

        with self.project:
            evalConfig = self.getConfig("evalOn")

            # try not to update dataversion, rather replace it completly
            if "dataversion" in evalConfig:
                self.project.config["dataversion"] = {}

            self.project.config.update(evalConfig)
            self.project.trigger("configChanged", None)
            dm = self.getData("metadata", list)

        return dm

    def getExternalTestset(self):
        """
        get a testset from a different dataset that is not currently loaded
        """
        with self.project:
            evalConfig = self.getConfig("evalOn")

            # try not to update dataversion, rather replace it completly
            if "dataversion" in evalConfig:
                self.project.config["dataversion"] = {}

            self.project.config.update(evalConfig)
            self.project.trigger("configChanged", None)
            split = self.getConfig("datasetSplit")
            dataset = self.project.getData(f"dataset-{split}", list)
            dm = self.getData("dataversionmanager")
            recordsMap = dm.getRecordsForSplit(split)

            # self.setConfig("datasetSplits", list(self.getConfig("dataversion.split", {}).keys()))
        return dataset, dm, recordsMap

    def getTestDataAndRecordMap(self):
        # load or generate test data and model state
        if bool(self.getConfig("evalOn", {})):
            testData, dm, recordsMap = self.getExternalTestset()
        else:
            testData = self.project.getData("dataset-test", list)
            dm = self.getData("dataversionmanager")
            recordsMap = dm.getRecordsForSplit("test")

        return testData, recordsMap

    def getTestData(self):
        return self.getTestDataAndRecordMap()[0]

    def getTestRecordMap(self):
        return self.getTestDataAndRecordMap()[1]

    def segmentEvaluation(self, name):
        """ "
        segment wise evaluation
        """
        testData = self.getTestData()
        self.setConfig("datasetSplits", list(self.getConfig("dataversion.split", {}).keys()))
        model = self.getModel()

        # setup data and scorer
        scorer = Scorer(classNames=self.getConfig("classification.classNames"), trace=True)
        # use the validation metrics also for evaluation
        scorer.metrics = self.getConfig("trainingParameter.validationMetrics")
        threshold = self.getConfig("fixedThreshold", False)
        scorer.threshold = threshold or self.getData("threshold", float)
        augmentAfterPredict = self.getConfig("dataAugmentationAfterPredict", False)
        da = DataAugmentation(self.project.config, "test", threshold=scorer.threshold)
        batchSize = self.getConfig("eval.batchSize")

        for data in tqdm(testData):
            x, truth = data
            # make sure to have a batchsize if batchSize == 1
            x = x.reshape(batchSize, x.shape[0], x.shape[1])
            truth = truth.reshape(batchSize, truth.shape[0], truth.shape[1])

            prediction = model.predict(x, get_likelihood=True)
            prediction, truth = da.augmentBatchesByConfig(prediction, truth, augmentAfterPredict)

            scorer.score(truth, prediction)

        result = scorer.scoreAllRecords()

        return scorer.recordResult, result

    def getExample(self, eventTrue, eventFalse, length):
        # remove ascending numbers (only starting points left)
        diff = np.diff(eventTrue)
        mask = np.hstack(([True], diff > 1))
        occurrences = eventTrue[mask]

        # random starting point
        start = np.random.choice(occurrences)
        # find endpoint
        end = eventFalse[eventFalse > start]
        end = end[0] if len(end) > 0 else length

        start = max(start, 0)
        end = min(end, length)

        return start, end

    def getExamples(self, truth, predictions):
        """ "
        get examples for each combination in the confusion matrix
        """
        classNames = self.getConfig("classification.classNames")

        examples = {}
        truth, predictions = truth.reshape(-1), predictions.reshape(-1)

        for i, name in enumerate(classNames):
            for j, name in enumerate(classNames):
                examples[f"{i}-{j}"] = []
                occurrences = np.where(truth == i)[0]
                occurrencesOthers = np.where(truth == j)[0]
                if any(occurrences) and any(occurrencesOthers):
                    for e in range(self.exampleCount):
                        example = self.getExample(occurrences, occurrencesOthers, len(truth))
                        examples[f"{i}-{j}"].append(example)

        return examples

    def eventEvaluation(self):
        """
        event evaluation, where events are computed using dataAugmentationAfterPredict defined in eventEval Configurations
        """
        recordResults, results = self.getData("evalResults", list)
        recordsMap = self.getTestRecordMap()

        with self.project:
            self.project.config.update(self.getConfig("eventEval"))
            scorer = EventScorer(classNames=self.getConfig("classification.classNames"), trace=True)

            scorer.majorityVote = self.getConfig("eventEval.tpStrat", "overlap") == "majority"
            scorer.noTN = self.getConfig("eventEval.tnStrat", "eventcount") == "noTN"

            threshold = self.getConfig("fixedThreshold", False)
            scorer.threshold = threshold or self.getData("threshold", float)
            scorer.metrics = self.getConfig("eval.metrics")
            scorer.metrics.append("confusion")
            augmentAfterPredict = self.getConfig("dataAugmentationAfterPredict", False)
            da = DataAugmentation(self.project.config, "test", threshold=scorer.threshold)

            metaData = pd.DataFrame(self.getTestMetadata())
            relMetaData = self.getConfig("eval.clinicalMetrics")
            metaData.replace("M", None, inplace=True)
            metaData["indexArousal"] = metaData["indexArousal"].astype(float)
            # metaData["countArousal"] = metaData["countArousal"].astype(float)

            resultRows = []
            lastDimension = self.getConfig("numClasses") if self.getConfig("model.oneHotDecoded") else 1
            for index, recordResult in tqdm(recordResults.items()):
                prediction, truth = (
                    recordResult["prediction"].reshape(1, -1, lastDimension),
                    recordResult["truth"].reshape(1, -1, 1),
                )
                prediction, truth = da.augmentBatchesByConfig(prediction, truth, augmentAfterPredict)
                prediction = prediction.reshape(-1)
                truth = truth.reshape(-1)
                recordId = recordsMap[index]
                results = {"recordId": recordId}
                recordData = metaData.query(f'recordId == "{recordId}"')
                recordData = recordData.iloc[0] if len(recordData) == 1 else {}

                tst = recordData["tst"] if "tst" in recordData else 0

                sleepDataTruth = SleepMetaData()
                sleepDataTruth.registerMetadata("tst", tst, "#/h")
                sleepDataTruth.fromArousalSignal(truth)
                # sleepDataTruth.fromApneaSignal(truth)

                sleepDataPredicted = SleepMetaData()
                sleepDataPredicted.registerMetadata("tst", tst, "#/h")
                sleepDataPredicted.fromArousalSignal(scorer.flattenPrediction(prediction))
                # sleepDataPredicted.fromApneaSignal(scorer.flattenPrediction(prediction))

                for metricName in relMetaData:
                    results[f"{metricName}-software"] = (
                        recordData[metricName].item()
                        if metricName in recordData and bool(recordData[metricName].tolist())
                        else 0
                    )
                    results[f"{metricName}-truth"] = sleepDataTruth.getMetaData(metricName)
                    results[f"{metricName}-prediction"] = sleepDataPredicted.getMetaData(metricName)

                r = scorer.score(truth, prediction)
                if len(r["confusion"]) == 2:
                    (tn, fn), (fp, tp) = r["confusion"]
                    del r["confusion"]
                    results["tn"] = tn
                    results["fn"] = fn
                    results["fp"] = fp
                    results["tp"] = tp
                results.update(r)
                results["examples"] = self.getExamples(truth, prediction)
                resultRows.append(results)
            result = scorer.scoreAllRecords()

        return resultRows, result

    def generateData(self, name):
        if name == "evalResults":
            segmentResults = self.segmentEvaluation(name)
            self.registerData("evalResults", segmentResults)
        elif name == "eventResults":
            eventResults = self.eventEvaluation()
            self.registerData("eventResults", eventResults)

    def main(self):
        self.getData("eventResults", list)
