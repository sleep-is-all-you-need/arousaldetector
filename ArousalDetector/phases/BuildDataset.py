import numpy as np
from pyPhases import Phase
from pyPhasesML.datapipes import Augmentor, DatasetXY, FoldMapper
from pyPhasesML import DataversionManager, DataLoader

from ArousalDetector.DataAugmentation import DataAugmentation


class BuildDataset(Phase):
    """
    in this phase the raw data is loaded from file, augmented and converted into a dataset that can be used for training
    """

    def buildDatasetAugmentorBySplit(self):
        split = self.getConfig("datasetSplit")
        memmapOptions = {
            "dtype": self.getConfig("preprocessing.dtype"),
        }

        fileSplit = split
        isFoldSplit = self.getConfig("hasFolds") and split in ["training", "validation"]
        if isFoldSplit:
            fileSplit = "trainval"
            self.setConfig("datasetSplit", fileSplit)

        dataExporterSignals = self.project.getData("data-processed", np.memmap, options=memmapOptions)
        dataExporterFeatures = self.project.getData("data-features", np.memmap, options=memmapOptions)
        dm = self.getData("dataversionmanager", DataversionManager)
        recordIds = dm.getRecordsForSplit(fileSplit)
        metadata = self.project.getData("metadata", list)
        metadataMap = {r["recordId"]: r for r in metadata}
        recordMetaData = [metadataMap[r] for r in recordIds]

        if isFoldSplit:
            fold = int(self.getConfig("fold"))
            length = len(dataExporterSignals)
            foldmapper = FoldMapper(length, self.getConfig("dataversion.folds"), fold)
            if split == "training":
                dataExporterSignals = foldmapper.getTraining(dataExporterSignals)
                dataExporterFeatures = foldmapper.getTraining(dataExporterFeatures)
                recordMetaData = foldmapper.getTraining(recordMetaData)
            else:
                dataExporterSignals = foldmapper.getValidation(dataExporterSignals)
                dataExporterFeatures = foldmapper.getValidation(dataExporterFeatures)
                recordMetaData = foldmapper.getValidation(recordMetaData)

        augSteps = "segmentAugmentationEval" if split != "training" else "segmentAugmentation"
        augmentationSteps = self.project.getConfig(augSteps)

        datapipe = DatasetXY(dataExporterSignals, dataExporterFeatures)
        da = DataAugmentation(self.project.config, split, recordMetadata=recordMetaData)
        return Augmentor(datapipe, da, augmentationSteps)

    def buildDatasetBySplit(self):
        split = self.getConfig("datasetSplit")
        batchSize = self.getConfig("trainingParameter.batchSize") if split != "test" else self.getConfig("eval.batchSize")
        buildConfig = self.getConfig("BuildDataset")

        augmentor = self.buildDatasetAugmentorBySplit()

        return DataLoader.build(
            augmentor,
            preload=buildConfig["useMultiThreading"],
            batchSize=batchSize,
            shuffle=self.getConfig("trainingParameter.shuffle", False),
            shuffleSeed=self.getConfig("trainingParameter.shuffleSeed", 2),
        )

    def generateData(self, name):
        isAugmentor = name[:9] == "augmentor"
        splitName = name[9:] if isAugmentor else name[8:]
        if splitName != "":
            self.setConfig("datasetSplit", splitName)

        # dont register the data
        return self.buildDatasetAugmentorBySplit() if isAugmentor else self.buildDatasetBySplit()

    def main(self):
        self.log("No dataset is build on default. There is no point running this phase directly.")
