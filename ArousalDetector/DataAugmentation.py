import numpy as np
from numpy.random import default_rng
from pyPhasesML import DataAugmentation as pyPhasesDataAugmentation


class DataAugmentation(pyPhasesDataAugmentation):
    """
    X: (numSegments, numSamples, numChannels)
    Y: (numSegments, ... )
    """

    def __init__(self, config, splitName, seed=2, threshold=None, recordMetadata=None) -> None:
        super().__init__(config, splitName)
        self.catMatrix = None
        self.numClasses = config["numClasses"]
        self.rng = default_rng(seed=2)
        self.threshold = threshold
        self.recordMetadata = recordMetadata

    def hotEncode(self, X, Y):
        Y = Y.astype(np.int32)
        shape = list(Y.shape)

        if self.catMatrix is None:
            self.catMatrix = np.eye(self.numClasses)
            # append a zero line on the end so that -1 will map to all zeros
            self.catMatrix = np.concatenate((self.catMatrix, np.eye(1, self.numClasses, -1))).astype(np.int32)
        Y = self.catMatrix[Y].reshape((shape[0], shape[1], self.numClasses))
        return X, Y

    def hotDecode(self, X, Y):
        Ynew = np.argmax(Y, axis=2)
        Ynew[Y.sum(axis=2) == 0] = -1
        return X, Ynew.reshape(Y.shape[0], Y.shape[1], 1)

    def znorm(self, X, Y):
        X = (X - X.mean(axis=1, keepdims=True)) / (X.std(axis=1, keepdims=True) + 0.000000001)
        return X, Y

    def MagScale(self, X, Y, low=0.8, high=1.25):
        scale = low + self.rng.random(1, dtype=X.dtype) * (high - low)
        X = scale * X

        return X, Y

    def channelShuffle(self, X, Y, channelSlice):
        channelSlice = slice(channelSlice[0], channelSlice[1])

        cutChannels = X[:, :, channelSlice].copy()
        self.rng.shuffle(cutChannels, axis=2)
        X[:, :, channelSlice] = cutChannels

        return X, Y

    def _fixeSizeSingleChannel(self, X, size, fillValue=0, position="center", startAt=0):
        newShape = list(X.shape)
        newShape[1] = size

        centerNew = size // 2
        signalSize = X.shape[1]
        centerSignal = signalSize // 2

        if startAt == 0 and position == "center":
            startAt = centerNew - centerSignal

        newX = np.full(newShape, fillValue, dtype=X.dtype)

        if startAt < 0:
            offsetStart = startAt * -1
            length = min(newShape[1], X.shape[1] - offsetStart)
            newX[:, :length, :] = X[:, offsetStart : offsetStart + newShape[1], :]
        else:
            newX[:, startAt : startAt + signalSize, :] = X

        return newX, startAt

    def fixedSize(self, X, Y, size, fillValue=0, fillValueY=-1, position="center"):
        X, startAt = self._fixeSizeSingleChannel(X, size, fillValue, position)
        Y, _ = self._fixeSizeSingleChannel(Y, size, fillValueY, position, startAt)
        return X, Y

    def fixedSizeX(self, X, Y, size, fillValue=0, position="center"):
        X, _ = self._fixeSizeSingleChannel(X, size, fillValue, position)
        return X, Y

    def fixedSizeY(self, X, Y, size, fillValue=0, position="center"):
        Y, _ = self._fixeSizeSingleChannel(Y, size, fillValue, position)
        return X, Y

    def restoreLength(self, X, Y, length):
        curLength = Y.shape[1]
        padLeft = (curLength - length) // 2

        return X, Y[:, padLeft : (padLeft + length), :]

    def selectChannel(self, X, Y, channel):
        return X[:, :, channel : channel + 1], Y

    def selectChannels(self, X, Y, channels):
        return X[:, :, channels], Y

    def selectChannelRandom(self, X, Y, channels):
        channel = self.rng.choice(channels)
        # keep selected channel and all other channels not listed
        return X[:, :, [i for i in range(X.shape[2]) if i == channel or i not in channels]], Y

    def changeType(self, X, Y, dtype):
        X = X.astype(dtype)
        Y = Y.astype(dtype)

        return X, Y

    def reduceY(self, X, Y, factor, reduce="mean"):
        """reduce prediction by factor and using the mean of the prediction, using majority for Y"""

        if X.shape[1] % factor != 0:
            cutOff = X.shape[1] % factor
            print(
                f"data not divisible by factor {factor}, cutting {cutOff} samples, might be because recording does not fit entirely in the window 2^^21"
            )
            X = X[:, :-cutOff, :]
            Y = Y[:, :-cutOff, :]

        X = X.reshape((X.shape[0], X.shape[1] // factor, factor, X.shape[2]))
        Y = Y.reshape(Y.shape[0], Y.shape[1] // factor, factor, Y.shape[2])

        if reduce == "mean":
            X = X.mean(axis=2)
            arr = Y.astype(np.int32) if isinstance(Y, np.ndarray) else Y.int()
            Y = np.apply_along_axis(lambda x: np.bincount(x).argmax(), axis=2, arr=arr)
        elif reduce == "max":
            X = X.max(axis=2)
            Y = Y.max(axis=2)

        return X, Y

    def deleteIgnored(self, X, Y):
        """reduce prediction by factor and using the mean of the prediction, using majority for Y"""
        yShape = Y.shape
        xShape = X.shape
        X = X.reshape(-1)
        Y = Y.reshape(-1)

        deleteIgnored = Y != -1
        X = X[deleteIgnored]
        Y = Y[deleteIgnored]

        return X.reshape(xShape[0], -1, xShape[2]), Y.reshape(yShape[0], -1, yShape[2])

    def deleteIgnoredMultiClass(self, X, Y):
        """reduce prediction by factor and using the mean of the prediction, using majority for Y"""
        numClasses = X.shape[2]
        yShape = Y.shape
        xShape = X.shape

        X = X.reshape(-1, numClasses)
        if Y.shape[2] == 1:
            Y = Y.reshape(-1)
            deleteIgnored = Y != -1
        else:
            Y = Y.reshape(-1, numClasses)
            s = Y.sum(axis=1)
            deleteIgnored = s != 0
        X = X[deleteIgnored]
        Y = Y[deleteIgnored]

        return X.reshape(xShape[0], -1, xShape[2]), Y.reshape(yShape[0], -1, yShape[2])

    def _reduction(self, Y, strategy="majority", options=None, ignoreZero=True):
        if strategy == "majority":
            bincounts = np.bincount(Y.astype(np.int32))
            return bincounts[1:].argmax() + 1 if ignoreZero else bincounts.argmax()
        elif strategy == "prefer":
            # check if options values exist
            for o in options:
                if o in Y:
                    return o
        else:
            raise ValueError(f"unknown strategy {strategy}")

    def _combineWithPatience(self, Y, patience=10, reduction="majority", reductionOptions=None):
        """reduce prediction by factor and using the mean of the prediction, using majority (of values > 0) for Y or a list of prefered values"""
        pos = np.where(Y == 0)[0]
        lengths = np.diff(pos)
        if len(pos) > 0:
            lengths = np.append(lengths, len(Y) - pos[-1])
            lastEventStart = 0
            gap = 0
            for index, (position, length) in enumerate(zip(pos, lengths)):
                isEvent = length > 1
                if isEvent:
                    # fill gaps with mode of previous and next event
                    if gap > 0 and gap < patience:
                        nextEventEnd = pos[index + 1] if index + 1 < len(pos) else len(Y)
                        value = self._reduction(Y[lastEventStart:nextEventEnd], reduction, reductionOptions)
                        Y[lastEventStart:nextEventEnd] = value
                    else:
                        # smooth last event
                        start = position + 1
                        end = position + length
                        value = self._reduction(Y[start:end], reduction, reductionOptions)
                        Y[start:end] = value
                    lastEventStart = position + 1
                    gap = 1
                else:
                    gap += 1
        return Y

    def _minLength(self, Y, minLength=10):
        # Find all event chains
        pos = np.where(np.diff(Y) != 0)[0] + 1
        if len(pos) > 0:
            chains = np.split(Y, pos)
            # Replace chains shorter than min_length with 0s
            for i in range(len(chains)):
                if len(chains[i]) < minLength and chains[i][0] != 0:
                    start = pos[i - 1] if i > 0 else 0
                    end = start + len(chains[i])
                    Y[start:end] = 0
        return Y

    def _reduceMultiEvent(self, Y, reduction="majority", reductionOption=None):
        changes = np.where(np.diff(Y) != 0)[0] + 1
        event_boundaries = np.split(Y, changes)
        position = 0
        events = []
        for segment in event_boundaries:
            events.append((segment[0], position, len(segment)))
            position += len(segment)

        lastEvent = [0, 0]
        mergeLastEvents = -1
        for classIndex, position, length in events:
            if lastEvent[0] > 0 and classIndex > 0:
                if mergeLastEvents == -1:
                    mergeLastEvents = lastEvent[1]
            elif mergeLastEvents >= 0:
                Y[mergeLastEvents:position] = self._reduction(Y[mergeLastEvents:position], reduction, reductionOption)
                mergeLastEvents = -1
            lastEvent = (classIndex, position)

        return Y

    def toEvent(
        self,
        X,
        Y,
        patience=10,
        minLength=3,
        frequency=50,
        recution="majority",
        reductionOption=None,
        nonePenalty=None,
        reduceMultiEvent=False,
    ):
        threshold = self.threshold
        patience = int(patience * frequency)
        minLength = int(minLength * frequency)

        xShape = X.shape
        if xShape[2] > 1:
            if nonePenalty == "threshold":
                nonePenalty = self.threshold
                X[:, :, 0] *= nonePenalty
            X = X.argmax(axis=2)
        else:
            X[X >= threshold] = 1
            X[X < threshold] = 0

        X = X.reshape(-1)
        X = self._combineWithPatience(X, patience, recution, reductionOption)

        if reduceMultiEvent:
            X = self._reduceMultiEvent(X, recution, reductionOption)

        if minLength > 0:
            X = self._minLength(X, minLength)

        X = X.reshape(xShape[0], xShape[1], 1)
        return X, Y

    def toArousal(self, X, Y, patience=10, minLength=3, frequency=50):
        return self.toEvent(X, Y, patience, minLength, frequency)

    def toApnea(self, X, Y, patience=10, minLength=10, frequency=50):
        return self.toEvent(X, Y, patience, minLength, frequency)

    def derive(self, X, Y, channels):
        newChan = X[:, :, channels[0]] - X[:, :, channels[1]]
        X = np.concatenate((X, newChan[:, :, np.newaxis]), axis=2)
        return X, Y

    def selectChannelBestMetadata(self, X, Y, channels, metadataFields):
        metadata = self.recordMetadata[self.currentIndex]
        metadata = [metadata[k] for k in metadataFields]
        bestIndex = np.argmax(metadata)
        return X[:, :, [i for i in range(X.shape[2]) if i == bestIndex or i not in channels]], Y
