import sys

from pyPhases.util.Logger import classLogger
from pypika import Table, PostgreSQLQuery
from pypika.queries import QueryBuilder



# TODO: split to TSMRecordDB!
@classLogger
class MedicalDB:
    connection = None
    client = None
    assocCursor = None
    _instance = None

    config = {
        "user": "root",
        "password": "example",
        "host": "localhost",
        "port": 5432,
        "database": "data",
    }

    @classmethod
    def get(cls) -> "MedicalDB":
        if cls._instance is None:
            cls._instance = cls.__new__(cls)
            cls._instance.connect()
        return cls._instance

    def connect(self):

        import psycopg2

        try:
            c = MedicalDB.config
            conn = psycopg2.connect(
                "host=%s port=%i dbname=%s user=%s password=%s"
                % (
                    c["host"],
                    c["port"],
                    c["database"],
                    c["user"],
                    c["password"],
                )
            )

        except psycopg2.Error as e:
            self.logError(f"Error connecting to Postgres Platform: {e}")
            sys.exit(1)
        self.conn = conn
        self.cursor = conn.cursor()

    def execute(self, sql, **vars):
        if isinstance(sql, QueryBuilder):
            sql = sql.get_sql()
        return self.cursor.execute(sql, **vars)

    def executeAndFetch(self, sql, **vars):
        self.execute(sql, **vars)
        return self.cursor.fetchall()

    def commit(self):
        self.conn.commit()
    
    def executeAndFetchOne(self, sql, **vars):
        self.execute(sql, **vars)
        return self.cursor.fetchone()

    def close(self):
        if self.connection is not None:
            self.connection.close()

    def fillRecordId(self, id):
        return id

    def getRecordData(self, recordId):
        acqu = Table("acquisitions")
        patients = Table("patients")
        q = (
            PostgreSQLQuery.from_("acquisitions")
            .select("patient_id", "case_id", "acquisition_date", "patient_age", patients.gender)
            .join(patients)
            .on(patients.patient_id == acqu.patient_id)
            .where(acqu.acquisition_id == recordId)
        )
        row = self.executeAndFetchOne(q)

        return row

    def getDiagnosis(self, case_id):

        dia = Table("diagnoses")
        q = (
            PostgreSQLQuery.from_("diagnoses")
            .select("determination_date", "diagnosis_type", "pri_icd_code", "main_or_secondary")
            .where(dia.case_id == case_id)
        )
        return self.executeAndFetch(q)

    def getDiagnoses(self, recordId):

        dia = Table("diagnosis_medicalReport")
        diagnoses_types = ["rls_plmd", "insomnie", "hypersomnie", "rbd", "sonstige", "andere_hinweise", "sbas_nachrdi_lt5_lt15", "diagnosen_rdi_lt5_lt15", "normalbefund"]
        q = (
            PostgreSQLQuery.from_(dia)
            .select(*diagnoses_types)
            .where(dia.recordid == recordId)
        )
        r = self.executeAndFetchOne(q)

        diagnoseCats = {c: r[i] for i, c in enumerate(diagnoses_types) if r is not None}
        diagnoseCats["diagnoses"] = ";".join(diagnoses for diagnoses in r if diagnoses is not None)  if r is not None else ""
        return diagnoseCats

    def getPSGType(self, recordId):

        dia = Table("diagnosis_medicalReport")
        q = (
            PostgreSQLQuery.from_(dia)
            .select("psg_type")
            .where(dia.recordid == recordId)
        )
        r = self.executeAndFetchOne(q)

        return r[0] if bool(r) else ""
