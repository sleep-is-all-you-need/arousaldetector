import csv
from pathlib import Path

from pyPhases.util.Logger import classLogger


@classLogger
class AliceTextReportLoader:
    txtEncoding = "latin-1"

    def getMetaDataList(self, reportfile, rowIds):
        data = []
        extracedIds = []
        with open(reportfile, newline="", encoding=AliceTextReportLoader.txtEncoding) as csvfile:
            rows = csv.reader(csvfile)
            for row in rows:
                id, value, description = row
                if id in rowIds:
                    extracedIds.append(id)
                    if value != "":
                        nameId = rowIds[id]
                        if rowIds[id] == "":
                            nameId = "alice%s" % (id)
                        try:
                            data.append((nameId, float(value), description))
                        except:
                            self.logWarning("Failed to convert folling Line to float %s:%s value: %s" % (reportfile, id, value))
        notFound = set(rowIds) - set(extracedIds)
        if len(notFound) > 0:
            self.logWarning("export of following ids not found: %s" % notFound)
        return data
    
    def getMetaData(self, reportFile):
        if Path(reportFile).is_file():
            data = self.getMetaDataList(reportFile, self.relevantRows)
            return {name: value for name, value, _ in data}
        return {}

    def addTxtReport(self, reportFile, record):
        if Path(reportFile).is_file():
            data = self.getMetaDataList(reportFile, self.relevantRows)
            record.addAnnotationByName("report", data, device="Alice", manual=True)

    # relevant rows where exported from the report files
    relevantRows = {
        "140": "",  # bmi (not present)
        "155": "",
        "207": "tib",
        "208": "spt",
        "209": "tst",
        "211": "waso",
        "213": "w",
        "214": "r",
        "215": "nr",
        "218": "sLatency",
        "219": "latencyN1",
        "221": "latencyN2",
        "223": "latencyN3",
        "228": "rLatency",
        "230": "sEfficiency",
        "251": "wSPT",
        "255": "percentageW",
        "260": "percentageR",
        "262": "n1",
        "265": "percentageN1",
        "267": "n2",
        "270": "percentageN2",
        "272": "n3",
        "275": "percentageN3",
        "350": "countApneaCentralTIB",
        "352": "",
        "362": "countApneaObstructiveTIB",
        "364": "",
        "374": "countApneaMixedTIB",
        "376": "",
        "386": "countHypopneTIB",
        "388": "",
        "450": "",
        "451": "",
        "452": "",
        "454": "",
        "455": "",
        "456": "",
        "457": "",
        "458": "",
        "460": "",
        "461": "",
        "462": "",
        "463": "",
        "464": "",
        "466": "",
        "467": "",
        "480": "",
        "481": "",
        "482": "",
        "484": "",
        "485": "",
        "487": "",
        "488": "",
        "489": "",
        "491": "",
        "492": "",
        "493": "",
        "494": "",
        "495": "",
        "497": "",
        "498": "",
        "499": "",
        "500": "",
        "501": "",
        "503": "",
        "504": "",
        "517": "",
        "518": "",
        "519": "",
        "521": "",
        "522": "",
        "524": "",
        "525": "",
        "526": "",
        "528": "",
        "529": "",
        "530": "",
        "531": "",
        "532": "",
        "534": "",
        "535": "",
        "536": "",
        "537": "",
        "538": "",
        "540": "",
        "541": "",
        "554": "",
        "555": "",
        "556": "",
        "558": "",
        "559": "",
        "650": "",
        "651": "",
        "652": "",
        "654": "",
        "655": "",
        "656": "",
        "657": "",
        "658": "",
        "660": "",
        "661": "",
        "662": "",
        "663": "",
        "664": "",
        "666": "",
        "667": "",
        "680": "",
        "681": "",
        "682": "",
        "683": "",
        "684": "",
        "685": "",
        "687": "countApneaCentral",
        "688": "countApneaObstructive",
        "689": "countApneaMixed",
        "691": "countHypopnea",
        "692": "countApneaHypopnea",
        "693": "maxCentralApneaDuration",
        "694": "maxObstructiveApneaDuration",
        "695": "maxMixedApneaDuration",
        "697": "maxHyponeaDuration",
        "698": "maxApneaHyponeaDuration",
        "699": "meanCentralApneaDuration",
        "700": "meanObstructiveApneaDuration",
        "701": "meanMixedApneaDuration",
        "703": "meanHyponeaDuration",
        "704": "meanApneaHyponeaDuration",
        "717": "indexCentralApnea",
        "718": "indexObstructiveApnea",
        "719": "indexMixedApnea",
        "720": "indexApnea",
        "721": "indexHypopnea",
        "722": "ahi",
        "724": "ahiRem",
        "725": "ahiNrem",
        "750": "meanHRWake",
        "751": "meanHRRem",
        "752": "meanHRNrem",
        "758": "",
        "759": "",
        "760": "",
        "839": "meanHrSleep",
        "843": "maxHrSleep",
        "849": "minHrSleep",
        "867": "spo2Lower80Rem",
        "868": "spo2Lower80NRem",
        "869": "spo2Lower80",
        "875": "spO2Lower90Rem",
        "876": "spO2Lower90NRem",
        "877": "spO2Lower90",
        "891": "meanSpO2Wake",
        "892": "meanSpO2Rem",
        "893": "meanSpO2Nrem",
        "894": "meanSpO2",
        "897": "",
        "899": "minSpO2",
        "900": "",
        "901": "",
        "902": "snore",
        "904": "countPlms",
        "906": "",
        "908": "indexPlms",
        "909": "",
        "911": "countLmInPlm",
        "912": "indexLmInPlmns",
        "939": "countPlmsArousal",
        "940": "indexPlmsArousal",
        "941": "countLmInPlmArousal",
        "942": "indexLmInPlmsArousal",
        "1233": "countRight",
        "1234": "positionRight",
        "1235": "percentageSleepRightTST",
        "1255": "countSupine",
        "1257": "percentageSleepSupineTST",
        "1256": "positionSupine",
        "1277": "countProne",
        "1278": "positionProne",
        "1279": "percentageSleepProneTST",
        "1289": "positionLeft",
        "1288": "countLeft",
        "1290": "percentageSleepLeftTST",
        "1300": "",
        "1301": "",
        "1302": "",
        "1303": "",
        "1304": "",
        "1305": "",
        "1306": "",
        "1307": "",
        "1308": "",
        "1309": "",
        "1310": "",
        "1311": "",
        "1320": "",
        "1321": "",
        "1322": "",
        "1323": "",
        "1324": "",
        "1325": "",
        "1326": "",
        "1327": "",
        "1328": "",
        "1329": "",
        "1330": "",
        "1331": "",
        "1332": "",
        "1333": "",
        "1334": "",
        "1335": "",
        "1336": "",
        "1337": "",
        "1338": "",
        "1339": "",
        "1340": "",
        "1425": "",
        "1502": "indexOxyDesatRem",
        "1503": "indexOxyDesatNrem",
        "1504": "indexOxyDesatTotal",
        "1520": "countOxyDesat",
        "1619": "percentageSpO2Lower80",
        "1627": "percentageSpO2Lower90",
        "1801": "",
        "1802": "",
        "1804": "",
        "1815": "",
        "1826": "",
        "1827": "",
        "1829": "",
        "1840": "",
        "1851": "",
        "1852": "",
        "1854": "",
        "1865": "",
        "1876": "",
        "1877": "",
        "1879": "",
        "1890": "",
        "1901": "",
        "1902": "",
        "1904": "",
        "1915": "",
        "1926": "",
        "1927": "",
        "1929": "",
        "1940": "",
        "1951": "",
        "1952": "",
        "1954": "",
        "1965": "",
        "1976": "",
        "1977": "",
        "1979": "",
        "1990": "",
        "2001": "",
        "2002": "",
        "2004": "",
        "2015": "",
        "2026": "",
        "2027": "",
        "2029": "",
        "2040": "",
        "2051": "",
        "2052": "",
        "2054": "",
        "2065": "",
        "2076": "",
        "2077": "",
        "2079": "",
        "2090": "",
        "2101": "",
        "2102": "",
        "2104": "",
        "2115": "",
        "2126": "",
        "2127": "",
        "2129": "",
        "2140": "",
        "5001": "",
        "5503": "",
        "6114": "",
        "6116": "",
        "6118": "",
        "6124": "",
        "6126": "",
        "6128": "",
        "6130": "",
        "6136": "",
        "6138": "",
        "6140": "",
        "6142": "",
        "6148": "",
        "6151": "",
        "6152": "",
        "6153": "",
        "6154": "",
        "6155": "",
        "6156": "",
        "6161": "",
        "6163": "countRera",
        "6164": "",
        "6165": "",
        "6166": "",
        "6167": "",
        "6168": "",
        "6173": "indexRera",
        "6174": "rdi",
        "6175": "rdiRem",
        "6176": "rdiNrem",
        "6251": "",
        "6252": "",
        "6253": "",
        "6254": "",
        "6255": "",
        "6256": "",
        "6257": "",
        "6258": "",
        "6259": "",
        "6266": "",
        "6267": "",
        "6268": "",
        "6269": "",
        "6270": "",
        "6271": "",
        "6272": "",
        "6273": "",
        "6274": "",
        "6275": "",
        "6276": "",
        "6277": "",
        "6284": "",
        "6285": "",
        "6286": "",
        "6287": "",
        "6288": "",
        "6289": "",
        "6290": "",
        "6291": "",
        "6292": "",
        "6293": "",
        "6294": "",
        "6302": "",
        "6303": "",
        "6304": "",
        "6351": "",
        "6352": "",
        "6353": "",
        "6354": "",
        "6355": "",
        "6356": "",
        "6357": "",
        "6358": "",
        "6359": "",
        "6366": "",
        "6367": "",
        "6368": "",
        "6369": "",
        "6370": "",
        "6371": "",
        "6372": "",
        "6373": "",
        "6374": "",
        "6375": "",
        "6376": "",
        "6377": "",
        "6384": "",
        "6385": "",
        "6386": "",
        "8137": "indexApneaRight",
        "8138": "indexHypopneaRight",
        "8181": "indexApneaProne",
        "8182": "indexHypopneaProne",
        "8192": "indexApneaLeft",
        "8193": "indexHypopneaLeft",
        "8143": "rdiRight",
        "8159": "indexApneaBack",
        "8160": "indexHypopneaBack",
        "8162": "countReraBack",
        "8164": "",
        "8165": "rdiBack",
        "8185": "rdiProne",
        "8196": "rdiLeft",
        "9295": "",
        "9910": "",
        "9976": "countArousal",
        "9977": "indexArousal",
        "9982": "",
        "10105": "",
        "10308": "",
        "10317": "",
        "10326": "",
        "10335": "",
        "10344": "",
        "10353": "",
        "10362": "",
        "10371": "",
        "10380": "",
        "10389": "",
        "10398": "",
        "10407": "",
        "10416": "",
        "10425": "",
        "40026": "",
        "40027": "",
        "40032": "",
        "40033": "",
        "40074": "",
        "40075": "",
        "40080": "",
        "40081": "",
    }
