from datetime import datetime

from pyPhases.util.Logger import classLogger


@classLogger
class DominoAnnotationLoader:
    headers = {
        "Signal ID": "id",
        "Start Time": "startTime",
        "Signal Type": "signalType",
        "Signal Type": "signalType",
        "Events list": "possibleValues",
        "Rate": "Rate",
        "Unit": "unit",
    }

    def __init__(self):
        self.state = "meta"

        self.id = ""
        self.startTime = None
        self.unit = None
        self.signalType = None
        self.valueMap = None
        self.possibleValues = None

        self.targetFrequency = 1
        self.events = []
        self.curOffset = 0
        self.curValue = None
        self.eventStart = 0
        self.distinct = True

    def wrapTime(self, timeInSeconds):
        return int(timeInSeconds) * self.targetFrequency

    def finishEvent(self):
        if self.curValue is not None and self.curValue != "A":
            value = self.valueMap[self.curValue] if self.valueMap is not None else self.curValue
            start = self.wrapTime(self.eventStart)
            if self.distinct:
                self.events.append((start, value))
            else:
                end = self.wrapTime(self.curOffset)
                self.events.append((start, "(%s" % value))
                self.events.append((end, "%s)" % value))

    def addContentLine(self, line):
        values = [v.strip() for v in line.split(";")]

        if self.signalType == "Discret":
            time, value = values
            # time = "%i.%i.%i "%(self.startTime.)
            # time = datetime.strptime(value, "%d.%m.%Y %H:%M:%S")
            if self.curValue is None or self.curValue != value:
                self.finishEvent()
                self.eventStart = self.curOffset

            self.curValue = value
            self.curOffset += 30

    def validateMeta(self):
        assert self.id is not None
        assert self.startTime is not None
        assert self.possibleValues is not None
        assert self.signalType is not None
        assert self.signalType == "Discret", "at the moment only Discret signalTypes are supported"

        if self.id == "SchlafProfil\profil":
            assert self.Rate == "30 s", "at the moment only 30 sec Schlafprofil is supported"
            assert self.possibleValues == ["N3", "N2", "N1", "Rem", "Wach", "Artefakt"], "only AASM Schlafprofil is supported"

    def parseMetaLine(self, line):
        type, value = [v.strip() for v in line.split(":", 1)]
        type = self.headers[type]

        if type == "startTime":
            value = datetime.strptime(value, "%d.%m.%Y %H:%M:%S")  # 19.11.2020 19:30:30
        elif type == "possibleValues":
            value = [v.strip() for v in value.split(",")]

        self.__setattr__(type, value)

    def parseLine(self, line):
        if line == "":
            self.state = "content"
            self.validateMeta()
        elif self.state == "meta":
            self.parseMetaLine(line)
        else:
            self.addContentLine(line)

    def parseLines(self, lines):
        for line in lines:
            self.parseLine(line.strip())
        self.finishEvent()

    @staticmethod
    def load(path, valueMap=None, targetFrequency=1):
        dl = DominoAnnotationLoader()
        dl.valueMap = valueMap
        dl.targetFrequency = targetFrequency

        with open(path, "r") as f:
            content = f.readlines()

        dl.parseLines(content)

        return dl
