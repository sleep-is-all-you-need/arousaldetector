from pathlib import Path
from ArousalDetector.recordloaders.DominoErgLoader import DominoErgLoader

from ArousalDetector.PSGEventManager import PSGEventManager
from pyPhasesRecordloader.recordLoaders.EDFRecordLoader import EDFRecordLoader
from .DominoAnnotationLoader import DominoAnnotationLoader


class RecordLoaderDomino(EDFRecordLoader):
    def getFileBasePath(self, recrdId):
        return self.filePath + "/" + recrdId

    def getFilePathSignal(self, recordId):
        return self.getFileBasePath(recordId) + "/" + recordId + ".edf"

    def getFilePathAnnotation(self, recordId):
        return self.getFileBasePath(recordId) + "/annotations/Schlafprofil.txt"
    
    def getErgPath(self, recordId):
        return self.getFileBasePath(recordId) + "/" + recordId + ".erg"

    def existAnnotation(self, recordId):
        return Path(self.getFilePathAnnotation(recordId)).exists()

    def loadAnnotation(self, recordId, fileName, valueMap=None):
        filePath = self.getFileBasePath(recordId) + "/" + fileName + ".txt"
        annotationLoader = DominoAnnotationLoader.load(filePath, valueMap, self.annotationFrequency)

        return annotationLoader.events


    def getMetaData(self, recordName):

        metaData = super().getMetaData(recordName)
        # metaData.update(self.getAliceLoader().getMetaData(self.getFilePathAnnotation(recordName)))
        metaData.update(DominoErgLoader().getMetaDataFromFile(self.getErgPath(recordName), DominoErgLoader.relevantRows))
        
        # lightOff = self.getXMLPath(self.metaXML, ["Acquisition", "Sessions", "Session", "LightsOff"])
        # off, on = self.getLightAnnotations()
        # metaData["lightOff"] = off
        # metaData["lightOn"] = on

        return metaData
    
    def getLightAnnotations(self):
        """get light off, on in seconds"""
        lightOff = self.getAnnotationTimeByName("Licht aus")
        lightOn = self.getAnnotationTimeByName("Ende der Messung")
        
        if lightOff is None:
            self.logError("Die Annotation 'Licht Aus' wurde nicht gefunden")
        else:
            lightOff = int(lightOff)
        if lightOn is None:
            self.logWarning("Die Annotation 'Ende der Messung' wurde nicht gefunden")
        else:
            lightOn = int(lightOn)

        return lightOff, lightOn
            

    def getEventArray(self, recordId, signalLengths, annotationFrequency):
        self.annotationFrequency = annotationFrequency

        eventArray = self.loadAnnotation(
            recordId,
            "SchlafProfil",
            {
                "N3": "N3",
                "N2": "N2",
                "N1": "N1",
                "Rem": "R",
                "Wach": "W",
                "Artefakt": "undefined",
            },
        )

        self.lightOff, self.lightOn = self.getLightAnnotations()

        return PSGEventManager().getEventSignalFromAnnotationMap(eventArray, signalLengths)
