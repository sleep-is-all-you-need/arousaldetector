Machine learning based sleep arousal detection.

# Setup

The following setup is required to run the project:
- Execution setup (2-10min)
- Dataset setup (optional ~3min)
- Trained model (optional ~36h)

## Execution setup

There are two ways for execution: either Docker or local setup.

# Local execution
## Requirements

- Clone Repository: `git clone https://gitlab.com/sleep-is-all-you-need/arousaldetector.git`
- Python `>= 3.8` and `< 3.12` (tested with `3.11.4`)
- [PyTorch](https://pytorch.org/get-started/locally/) (tested with `v2.0.1`, if you want to use newer version, use a compatible numpy requirement)
- packages are defined in `requirements.txt`, for installation run `pip install -r requirements.txt`

You can now use the `phases` command to run the project (use `python -m phases` if the path isn't setup correctly). Test it by running `phases --version`.


# Docker

A docker image with all dependencies is available: `registry.gitlab.com/sleep-is-all-you-need/arousaldetector/torch`
How to run the image is described below.

## Requirements

[Docker](https://www.docker.com/get-started/)

## Docker compose

Create a file `docker-compose.yml` with following content:


```yaml
version: "3.7"

services:
  phases:
    image: registry.gitlab.com/sleep-is-all-you-need/arousaldetector/torch
    entrypoint: python -m phases
    volumes:
      - ./data:/app/data
      - ./logs:/app/logs
      # you can use this to link your local datasets, here it is assumed
      # that the datasets are in th ./datasets/ (p.e. ./datasets/shhs/) folder
      # in the same location where the docker-compose.yml file is
      - ./datasets/:/datasets
      # a link for your custom configs
      - ./userconfigs:/app/userconfigs
    # required for gpu execution using a nvidia gpu
    deploy:
      resources:
        reservations:
          devices:
            - driver: nvidia
              # this is the id of the nvidia gpu (default 0 can be 1 if multiple gpus 
              # or integrated gpu exist )
              device_ids: [ '0' ]
              capabilities: [ gpu ]
```
Create a local config file in `userconfigs/local.yml` and add the following content:

```yaml
shhs-path: /datasets/SHHS/
mros-path: /datasets/mros/
mesa-path: /datasets/mesa/
tsm-path: /datasets/dsds/
```

Now you can run the `phases` command using `docker compose run phases` (use `docker-compose` for older docker versions). Test it by running `docker compose run phases --version`.


## Dataset

For training a model, at least one dataset is required. For testing the pipeline, a generated dataset can be used see [Test the setup and single components](#test-the-setup-and-single-components-10-minutes). The datasets can be accessed via the [National Sleep Research Resource](https://sleepdata.org/datasets/). Supported datasets and their identifiers:

- [SHHS](https://sleepdata.org/datasets/shhs): `shhs` (SHHS1)
- [MESA](https://sleepdata.org/datasets/mesa): `mesa`
- [MrOs](https://sleepdata.org/datasets/MrOs/): `mros`
- DSDS: `DSDS` (non-public dataset from [Universitätsklinikum Carl Gustav Carus Dresden](https://www.uniklinikum-dresden.de/de))

Save the datasets you want to use in their original form in a folder, for example: `/datasets/SHHS/`.

## Setup datasets

Depending on the dataset, following config values need to be specified: `shhs-path`, `mros-path`, `mesa-path`, `tsm-path`

It is recommended that you create a folder `userconfigs` and add the file `userconfigs/local.yml` with following content:

```yaml
shhs-path: /datasets/shhs/
mros-path: /datasets/mros/
mesa-path: /datasets/mesa/
tsm-path: /datasets/DSDS/records
# windows path also supported: shhs-path: C:\datasets\shhs\
```
You need to update the path to your local datasets paths when using local execution.

## Setup pretrained models

The pretrained files are expected to be in the `data` directory, make sure the data directory does not exist or is empty and run:

`git clone https://gitlab.com/sleep-is-all-you-need/arousaldetector-pretrained.git data`


## Reproduction (training + evaluation)

Follow the setup steps and use either the local setup (use `phases ...` or `python -m phases`) or the docker compose setup (use `docker compose run phases ...`) to run following commands.

The training step needs the datasets to be downloaded and configured in the file `userconfigs/local.yml`.
The evaluation step requires the training to have run successfully or the pre-trained models.

### Training

For training, a Nvidia GPU with at least 24GB memory is highly recommended. This step will also create the preprocessed data.

- SHHS: `phases -c userconfigs/local.yml,configs/datasets/shhs/init.yaml run Training`
- MESA: `phases -c userconfigs/local.yml,configs/datasets/mesa/init.yaml run Training`
- MrOs: `phases -c userconfigs/local.yml,configs/datasets/MrOs/init.yaml run Training`
- DSDS: `phases -c userconfigs/local.yml,configs/datasets/tsm/init.yaml run Training`

The Error `Exception: filelist is empty` occurs when the recordloader could not find the dataset or PSG files. Make sure you downloaded the dataset and that the path is correct.

This step will run:
- load metadata (~1.5 hours for shhs)
- preprocess the training/validation files (~4 hours for shhs).
- training the model (~48h for shhs)
- results are saved in the `logs` folder


### Evaluation

For evaluation, a Nvidia GPU with at least 8GB memory is recommended.

| trained on | tested on | Command                                                                                                            |
|------------|-----------|--------------------------------------------------------------------------------------------------------------------|
| SHHS       | SHHS      | `phases -c userconfigs/local.yml,configs/datasets/shhs/init.yaml,configs/datasets/shhs/evalOn.yaml run EvalReport` |
| SHHS       | MESA      | `phases -c userconfigs/local.yml,configs/datasets/shhs/init.yaml,configs/datasets/mesa/evalOn.yaml run EvalReport` |
| SHHS       | MrOs      | `phases -c userconfigs/local.yml,configs/datasets/shhs/init.yaml,configs/datasets/mros/evalOn.yaml run EvalReport` |
| MESA       | SHHS      | `phases -c userconfigs/local.yml,configs/datasets/mesa/init.yaml,configs/datasets/shhs/evalOn.yaml run EvalReport` |
| MESA       | MESA      | `phases -c userconfigs/local.yml,configs/datasets/mesa/init.yaml,configs/datasets/mesa/evalOn.yaml run EvalReport` |
| MESA       | MrOs      | `phases -c userconfigs/local.yml,configs/datasets/mesa/init.yaml,configs/datasets/mros/evalOn.yaml run EvalReport` |
| MrOs       | SHHS      | `phases -c userconfigs/local.yml,configs/datasets/mros/init.yaml,configs/datasets/shhs/evalOn.yaml run EvalReport` |
| MrOs       | MESA      | `phases -c userconfigs/local.yml,configs/datasets/mros/init.yaml,configs/datasets/mesa/evalOn.yaml run EvalReport` |
| MrOs       | MrOs      | `phases -c userconfigs/local.yml,configs/datasets/mros/init.yaml,configs/datasets/mros/evalOn.yaml run EvalReport` |
| DSDS       | SHHS      | `phases -c userconfigs/local.yml,configs/datasets/tsm/init.yaml,configs/datasets/shhs/evalOn.yaml run EvalReport`  |
| DSDS       | MESA      | `phases -c userconfigs/local.yml,configs/datasets/tsm/init.yaml,configs/datasets/mesa/evalOn.yaml run EvalReport`  |
| DSDS       | MrOs      | `phases -c userconfigs/local.yml,configs/datasets/tsm/init.yaml,configs/datasets/mros/evalOn.yaml run EvalReport`  |

If DSDS Dataset is available:
| trained on | testet on | Command                                                                                                           |
|------------|-----------|-------------------------------------------------------------------------------------------------------------------|
| DSDS       | DSDS      | `phases -c userconfigs/local.yml,configs/datasets/tsm/init.yaml,configs/datasets/tsm/evalOn.yaml run EvalReport`  |
| SHHS       | DSDS      | `phases -c userconfigs/local.yml,configs/datasets/shhs/init.yaml,configs/datasets/tsm/evalOn.yaml run EvalReport` |
| MESA       | DSDS      | `phases -c userconfigs/local.yml,configs/datasets/mesa/init.yaml,configs/datasets/tsm/evalOn.yaml run EvalReport` |
| MrOs       | DSDS      | `phases -c userconfigs/local.yml,configs/datasets/mros/init.yaml,configs/datasets/tsm/evalOn.yaml run EvalReport` |


This step will run:
- threshold optimization (~2h and requires training data (~4h) and pre-trained model (see Training) if not allready loaded)
- load metadata (~1.5 hours for shhs, if not already loaded)
- extract testset (~1.5 hours for shhs, if not already loaded)
- record predictions (~45 min for shhs with gpu)
- Scoring Segments/Events (~15 min for shhs)
- results are saved in the `eval` folder

# Test the setup and single components (~10 minutes)

To test the whole pipeline at once with generated data run:

`phases run -c configs/datasets/test/init.yaml,configs/models/unet-min.yaml EvalReport`

<details>
  <summary>Expected Output Form: Load Metadata, Extract Data, Training, Evaluation</summary>

  ```console
  [phases] Phases v0.0.0 with pyPhases v0.0.0 (Log level: LogLevel.INFO)
  ... Some Run informations like
  [Run] Dependency 'dataBase' for Data could not be found in any config or other defined data
  ...
  [DataversionManager] All records are unique and present in the dataset splits
  ... Extracting Generated Records to a file:
  18%|███████████████████████████████████████████▉                                                                                                                                                                                                          | 10/56 [00:00<00:03, 15.15it/s][MemmapRecordExporter] saved data-processedTest-patient-97b2b652-trainval-0499b49d--current: Shape [    1 49500     2] (99 records)
  [MemmapRecordExporter] saved data-featuresTest-patient-97b2b652-trainval-0499b49d--current: Shape [    1 49500     1] (99 records)
  ... Training start
  [ConfigUnet] Total trainable Parameters: 2602
  ... or for arousal u-net
  [ConfigUnet] Total trainable Parameters: 37587782
  [Training] UNet(
    (conv_layer): LayersComposition(
      (layers): Sequential(
        (layer0): ConvLayer(
  ... Model Architecture
  [Project] save config from file logs/Test-patient-Test-97b2b652-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-0/model.config
  [Project] save config from file logs/Test-patient-Test-97b2b652-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-0/project.config
  ... Training output
  EPOCH 4: 100%|████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 134/134 [00:13<00:00, 10.23it/s, loss=0.11, lr=1.82e-5]
  Validation: 100%|█████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████████| 33/33 [00:03<00:00,  9.76it/s, auprc=0.0362, f1=0, auroc=0.0959, kappa=0, accuracy=0.966, eventCountDiff=3]
  [CheckPoint] Validation-Epoch Number: 5  Epoche Time: 77.45791409400044  Validation Time: 80.86243123700024
  ------------------
    47817 |       0
    1683 |       0
  ------------------
  [CheckPoint] Training Stats: loss:0.1102026053773823 | lr:1.819029850746269e-05 
  [CheckPoint] auprc: 0.032 [best: 0.040] f1: 0.000 [best: 0.000] auroc: 0.255 [best: 0.571] kappa: 0.000 [best: 0.000] accuracy: 0.966 [best: 0.966] eventCountDiff: 3.000 [best: 3.000]
  [CheckPoint] Model not improving since 3 epochs
  [SystemCheckPoint] Saving checkpoint
  [ConfigUnet] load best Model: logs/Test-patient-Test-97b2b652-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-1///checkpointModel_2_0.040_0.000_0.524_0.000_0.966_3.000.pkl
  [Training] Model trained and saved to logs/Test-patient-Test-97b2b652-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-1/
  [Training] Model trained auprc: 0.040118715801657746
  ...
  [EvalReport] RUN phase EvalReport: EvalReport
  [EvalReport] result segments: {'all_values': array([0, 0, 0, ..., 0, 0, 0]), 'pos_values': array([0, 0, 0, ..., 0, 0, 0]), 'ign_values': array([0, 0, 0, ..., 0, 0, 0]), 'neg_values': array([0, 0, 0, ..., 0, 0, 0]), 'auprc': 0.0545902382782475, 'auroc': 0.5006086779040451, 'confusion': array([[4.7396e+04, 2.0000e+00],
       [2.5950e+03, 7.0000e+00]]), 'f1-1': 0.005361930294906166, 'f1': 0.005361930294906166, 'accuracy': 0.94806, 'kappa': 0.00500496677613127, 'eventCountDiff': 1.0, 'eventCountDiff-q1': 1.0, 'eventCountDiff-q3': 1.0}
  [Training] Set the inputshape to [512, 2]
  ...
  [EvalReport] result events: {'all_values': array([1000,    0,    0, ...,    0,    0,    0]), 'pos_values': array([50,  0,  0, ...,  0,  0,  0]), 'ign_values': array([0, 0, 0, ..., 0, 0, 0]), 'neg_values': array([950,   0,   0, ...,   0,   0,   0]), 'auprc': 0.05, 'auroc': 0.5, 'confusion': array([[140,   0],
        [ 50,   0]]), 'f1-1': 0.0, 'f1': 0.0, 'accuracy': 0.736842105263158, 'kappa': 0.0, 'eventCountDiff': 0.0, 'eventCountDiff-q1': 0.0, 'eventCountDiff-q3': 1.0}
  [EvalReport] Results: {'f1': 0.0, 'acc-segment': 0.94806, 'acc': 0.736842105263158, 'kappa-segment': 0.00500496677613127, 'kappa-event': 0.0}
  [Project] save config from file eval/Test-Test-395bc6fd-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-0-e7f3dd7f-f1-bf21a9e8-76fbdb0c-75e37e4e-e7f3dd7f-456c65bb/project.config
  [EvalReport] Eval finished: eval/Test-Test-395bc6fd-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-0-e7f3dd7f-f1-bf21a9e8-76fbdb0c-75e37e4e-e7f3dd7f-456c65bb/
  ```
</details>

- all generated data is writtin into the `data` folder
- Ater Training the folder: `logs/Test-Test-395bc6fd-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-95bd2a6f-0/` exists with trainings logs and trained models.
- After Evaluation the folder:  `eval/Test-Test-395bc6fd-0499b49d-ConfigUnet-3d741778-b1d31e50-75e37e4e-0f99ee16-0-e7f3dd7f-f1-bf21a9e8-76fbdb0c-75e37e4e-e7f3dd7f-456c65bb`

## Test training pipeline on generated data

If a Nvidia GPU is installed it needs to have at least 24GB of memory else CPU execution or the minimal u-net is required.

### Using minimal u-net (less gpu memory required and faster)
`phases run -c configs/datasets/test/init.yaml,configs/models/unet-min.yaml Training`

### Using arousal u-net
`phases run -c configs/datasets/test/init.yaml Training`

## Test evaluation on test models

This will also train the model if it doesnt exist allready

`phases run -c configs/datasets/test/init.yaml,configs/models/unet-min.yaml EvalReport`

## Test extraction for specific dataset (dataset required)

This will load all metadata (dataset is required) from the shhs dataset and shhs recordings.
`phases run -c configs/datasets/shhs/init.yaml LoadData`

## Reproduce SHHS results (dataset required + pre-trained models required)

`phases run -c configs/datasets/shhs/init.yaml EvalReport`


# Development

This project uses the [pyPhases](https://pypi.org/project/pyPhases/) framework.

## Change dataset

make sure the right paths are set in `project.yaml` to use the dataset.

`phases run -c configs/datasets/[DATASET]/init.yaml run Training`

for example:

`phases run -c configs/datasets/mesa/init.yaml run Training`


## Change model

Add a new file with `ArousalDetector/models/[ModelName]/[ModelName].py` create a class `[ModelName]` and inherit it from `pyPhasesML.adapter.ModelTorchAdapter`. See our model for example `ArousalDetector/models/ConfigUnet/ConfigUnet.py`. Finally change the config value `model` to `[ModelName]`.

## Add new metric to scoring

Adding a new metric for the validation/evaluation, you register a new metric using the Scorer class from `pyPhasesML`.

```python
from pyPhasesML import Scorer
# ...
class Setup(Phase):
    def prepareConfig(self):
        # ...

        def scoreRecord(truth, prediction):
            # calculate the score for single record
            return 1.0

        def scoreAllSamples(truth, prediction):
            # calculate the score for all samples
            return 1.0

        Scorer.registerMetric("score1", scoreRecord, scoreAllSamples)
```

Update config:

```yaml
# add metric to training validation

trainingParameter:
  validationMetrics:
    - accuracy
    - score1
# add metric to evaluation

eval:
  metrics:
  - kappa
  - score1
```

## Run tests

`phases -c tests/config.yml test ./tests`

Using docker: `docker compose -f docker-compose-test.yml run --rm phases-test test ./tests`

## Create debug docker container

`docker compose run --rm --entrypoint bash phases-test`

## pyPhase structure
| Eval                               | Training with pyPhases                               |
|------------------------------------|------------------------------------------------------|
| ![Structure](assets/structure.svg) | ![PyPhases-Structure](assets/structure-pyPhases.svg) |

