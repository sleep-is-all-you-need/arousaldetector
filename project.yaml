name: ArousalDetector
namespace: ibmt.tud

# load machine learning plugin
plugins:
  - pyPhasesML
  - pyPhasesRecordloaderSHHS
  - pyPhasesRecordloaderMrOS
  - pyPhasesRecordloaderMESA
  - pyPhasesRecordloader

exporter:
  # load PickleExporter for handling default serializable data like primitive types and numpy-arrays
  - PickleExporter
  # load NumpyStreamExporter for handling complex records in memmaps
  # - name: RecordNumpyMemmapExporter
  #   basePath: data/
  - name: MemmapRecordExporter
    basePath: data/
  
phases:
    - name: Setup
      description: Prepare the project and compute neccessary config values
    - name: LoadData
      description: Load all RecordIDs from datasets
      exports:
        - allDBRecordIds
        - metadata
    - name: Extract
      description: Preprocess and save the raw data
      exports:
        - data-processed
        - data-features
        - removedRecordIndexes
        - dataversionmanager
    - name: BuildDataset
      description: Combine datasets and augment the data
      exports:
        - dataset-trainval 
        - dataset-training
        - dataset-validation
        - dataset-test
        - dataset-bySplit
    - name: Training
      description: Train model
      exports:
        - modelState
        - modelStateConfig
    - name: ThresholdOptimisation
      description: Threshold optimisation by optimising the threshold metric on validation data
      exports:
        - threshold
        - validationResult
    - name: Eval
      description: Evalutate the model against the test data
      exports:
        - evalResults
        - eventResults
    - name: EvalReport
      description: generate evaluation report
    - name: DataAnalysis
      description: Get class distribution and other statistics
      exports:
        - datasetstats
    - name: DataReport

data:
  - name: allDBRecordIds
    dependsOn:
      - dataBase
      - dataversion.recordIds
      - dataversion.groupBy
  - name: events
    dependsOn:
      - allDBRecordIds
  - name: eventDF
    dependsOn:
      - allDBRecordIds
  - name: metadata
    dependsOn:
      - allDBRecordIds
      - dataversion # not optimal
  - name: data-processed
    dependsOn:
      - allDBRecordIds
      - dataversion
      - datasetSplit
      - preprocessing
  - name: data-features
    dependsOn:
      - data-processed
  - name: dataversionmanager
    dependsOn:
      - data-processed
      - fold
  - name: removedRecordIndexes
    dependsOn:
      - data-processed
  - name: modelState
    dependsOn:
      - dataset-bySplit
      - modelName
      - model
      - inputShape
      - segmentAugmentation
      - trainingParameter
      - fold
  - name: modelStateConfig
    dependsOn:
      - modelState
  - name: threshold
    dependsOn:
      - modelState
      - eventEval
      - thresholdMetric
      - fixedThreshold
  - name: validationResult
    dependsOn:
      - modelState
      - segmentAugmentationEval
      - thresholdMetric
      - optimizeOn
  - name: evalResults
    dependsOn:
      - threshold
      - evalOn
      - dataAugmentationAfterPredict
      - segmentAugmentationEval # is applied to the test dataset
  - name: eventResults
    dependsOn:
      - evalResults
      - eventEval
      - eval.clinicalMetrics
  - name: dataset-bySplit
    dependsOn:
      # does not require the split, since its encoded in the name and will not be saved
      - allDBRecordIds
      - dataBase
      - dataversion
      - preprocessing
  - name: dataset-training
    dependsOn:
      - dataset-bySplit
      - fold
  - name: dataset-validation
    dependsOn:
      - dataset-bySplit
      - labelChannels
      - fold
  - name: dataset-trainval
    dependsOn:
      - dataset-bySplit
      - labelChannels
      - fold
  - name: dataset-test
    dependsOn:
      - dataset-bySplit
      - evalOn
  - name: datasetstats
    dependsOn:
      - data-processed
      - data-features

# import config files after this file is parsed
importAfter:
  - configs/preprocessing.yaml
  - configs/aasm-arousal.yaml
  - configs/model.yaml
  - configs/datasets/tsm/loader.yaml
  - configs/datasets/tsm-domino/loader.yaml

config:
  physionet-path: H:/physionet/
  shhs-path: H:/SHHS/
  mros-path: D:/Daten/datasets/MrOs/mros/
  mesa-path: D:/Daten/datasets/MESA/
  tsm-path: D:/Daten/TSM/records
  tsm-domino-path: D:/Daten/TSM/records
  evalOn: {}
  reportfolder: eval/data-report
    
  predict:
    shhs:
      model: pretrained/modelStateSHHS1-SHHS1-6cc5614f-1c712d23-ConfigUnet-daa1bc8d-4271f1e2-94f781b2-95bd2a6f-0--current
      config: pretrained/modelStateConfigSHHS1-SHHS1-6cc5614f-1c712d23-ConfigUnet-daa1bc8d-4271f1e2-94f781b2-95bd2a6f-0--current

  eval:
    batchSize: 1
    metrics:
    - auprc
    - f1
    - auroc
    - kappa
    - accuracy
    - eventCountDiff
    clinicalMetrics:
      - countArousal
      - indexArousal # requires tst in metadata
  endFold: 1
  fixedThreshold: null
  optimizeOn: validation # training, trainval
  # fixedThreshold: 0.22875
  eventEval:
    dataAugmentationAfterPredict: # postprocessing
    - name: deleteIgnoredMultiClass
    - name: reduceY
      factor: 50
    - name: toEvent
      patience: 3
      minLength: 15 # 2,10 addition + 3 sec min arousal
      frequency: 1

  dataAugmentationAfterPredict:
    - name: deleteIgnoredMultiClass


  
  medicalDB:
    host: localhost
    port: 3306
    user: mydbuser
    password: mydbpassword
    database: medicalDB

  evalChannelNames: [eeg, eog, emg]

  BuildDataset:
    useMultiThreading: True
    pinMemory: True
    threads: 2

  Extract:
    useMultiThreading: True
  Eval:
    allowTraining: True
  ThresholdOptimisation:
    allowTraining: True
