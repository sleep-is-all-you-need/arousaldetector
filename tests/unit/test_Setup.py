from pyPhases.test.TestCase import TestCase

from ArousalDetector.phases.Setup import Setup


class TestSetup(TestCase):
    phase = Setup()
    
    def testConfigValues(self):
        self.assertEqual(self.getConfig("numClasses"), 2)
        self.assertEqual(self.getConfig("fold"), 0)
        self.assertEqual(self.getConfig("datasetSplits"), [])
        self.assertEqual(self.getConfig("hasFolds"), False)