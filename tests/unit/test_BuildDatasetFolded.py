from unittest.mock import MagicMock

import numpy as np
import numpy.testing as npt

from ArousalDetector.phases.BuildDataset import BuildDataset
from pyPhases.test.Mocks import OverwriteConfig
from pyPhases.test.TestCase import TestCase
from pyPhasesML import SignalPreprocessing
from pyPhasesRecordloader import RecordLoader, RecordSignal, Signal


class TestBuildDataset(TestCase):
    phase = BuildDataset()

    C1 = np.array([1, 2, 3, 4, 5])
    C2 = np.array([6, 7, 8, 9, 10])
    C3 = C2

    def config(self):
        return {
            "dataversion": {
                "seed": None,
                "split": {
                    "trainval": ["0:8"],
                    "test": ["8:10"],
                },
                "folds": 4,
                "recordIds": ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"],
                "groupBy": None,
            },
            "preprocessing": {
                "targetFrequency": 1,
                "stepsPerType": {"test": ["add1"]},
                "targetChannels": ["C1", "C2", "C3"],
                "dtype": "float16",
                "forceGapBetweenEvents": False,
                "extendEvents": {"arousal": [0, 1], "arousal_rera": [0, 0]},
                "fillLastSleepstage": False,
            },
            "segmentAugmentation": [{"name": "fixedSize", "position": "center", "size": 10}],
            "segmentAugmentationEval": [{"name": "fixedSize", "position": "center", "size": 10}],
            "evalOn": None,
        }


    def setUp(self):
        super().setUp()

        class MyPreprocessing(SignalPreprocessing):
            def add1(signal: Signal, recordSignal: RecordSignal, config: dict):
                signal.signal += 1

        self.preprocessing = MyPreprocessing(self.getConfig("preprocessing"))

        dataProcessed = np.array(
            [
                self.C1.reshape(-1, 1),
                self.C2.reshape(-1, 1),
            ]
        )

        dataFeatures = np.array(
            [
                np.array([0, 0, 1, 1, 0]).reshape(-1, 1),
                np.array([0, 1, 0, 0, 0]).reshape(-1, 1),
            ]
        )

        self.project.registerData("metadata", [{"recordId": str(r)} for r in range(10)])
        RecordLoader.getRecordList = MagicMock(return_value=["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"])
        self.project.setConfig("datasetSplit", "test")
        self.project.registerData("data-processed", dataProcessed)
        self.project.registerData("data-features", dataFeatures)

        self.project.setConfig("datasetSplit", "trainval")
        self.project.registerData("data-processed", np.array([np.arange(5).reshape(-1, 1) + i for i in range(10)]))

        self.project.registerData(
            "data-features",
            np.array(
                [
                    np.array([0, 0, 0, 0, 0]).reshape(-1, 1),
                    np.array([0, 0, 0, 0, 1]).reshape(-1, 1),
                    np.array([0, 0, 0, 1, 0]).reshape(-1, 1),
                    np.array([0, 0, 0, 1, 1]).reshape(-1, 1),
                    np.array([0, 0, 1, 0, 0]).reshape(-1, 1),
                    np.array([0, 0, 1, 0, 1]).reshape(-1, 1),
                    np.array([0, 0, 1, 1, 0]).reshape(-1, 1),
                    np.array([0, 0, 1, 1, 1]).reshape(-1, 1),
                    np.array([0, 1, 0, 0, 0]).reshape(-1, 1),
                    np.array([0, 1, 0, 0, 1]).reshape(-1, 1),
                ]
            ),
        )

        self.project.setConfig("datasetSplit", "validation")
        self.project.registerData("data-processed", dataProcessed)
        self.project.registerData("data-features", dataFeatures)

    @OverwriteConfig({"datasetSplit": "test"})
    def testDataTest(self):
        data = iter(self.getData("dataset-test"))

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 1, 2, 3, 4, 5, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 1, 0, -1, -1])

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 6, 7, 8, 9, 10, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 1, 0, 0, 0, -1, -1])

    def testDataTraining(self):
        dataset = self.getData("dataset-training")
        
        self.assertEqual(len(dataset), 8)
        
        data = iter(dataset)
        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 2, 3, 4, 5, 6, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 3, 4, 5, 6, 7, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 1, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 4, 5, 6, 7, 8, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 5, 6, 7, 8, 9, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 1, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 6, 7, 8, 9, 10, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 1, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 7, 8, 9, 10, 11, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 1, 1, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 8, 9, 10, 11, 12, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 1, 0, 0, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 9, 10, 11, 12, 13, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 1, 0, 0, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)

        data = iter(dataset)
        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 2, 3, 4, 5, 6, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 0, -1, -1])

    @OverwriteConfig(fold=4)
    def testDataTrainingFold4(self):
        data = iter(self.getData("dataset-training"))

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 0, 1, 2, 3, 4, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 0, 0, -1, -1])

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 1, 2, 3, 4, 5, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 0, 1, -1, -1])

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 2, 3, 4, 5, 6, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 3, 4, 5, 6, 7, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 1, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 4, 5, 6, 7, 8, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 5, 6, 7, 8, 9, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 1, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 6, 7, 8, 9, 10, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 1, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 7, 8, 9, 10, 11, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 1, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)

    def testDataValidation(self):
        data = iter(self.getData("dataset-validation"))

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 0, 1, 2, 3, 4, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 0, 0, -1, -1])

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 1, 2, 3, 4, 5, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 0, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)

    @OverwriteConfig(fold=1)
    def testDataValidationFold1(self):
        data = iter(self.getData("dataset-validation"))

        x, y = next(data)
        npt.assert_equal(x.reshape(-1), [0, 0, 0, 2, 3, 4, 5, 6, 0, 0])
        npt.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 3, 4, 5, 6, 7, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 0, 1, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)

    @OverwriteConfig(fold=2)
    def testDataValidationFold2(self):
        data = iter(self.getData("dataset-validation"))

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 4, 5, 6, 7, 8, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 5, 6, 7, 8, 9, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 0, 1, 0, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)

    @OverwriteConfig(fold=4)
    def testDataValidationFold4(self):
        data = iter(self.getData("dataset-validation"))

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 8, 9, 10, 11, 12, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 1, 0, 0, 0, -1, -1])

        x, y = next(data)
        np.testing.assert_equal(x.reshape(-1), [0, 0, 0, 9, 10, 11, 12, 13, 0, 0])
        np.testing.assert_equal(y.reshape(-1), [-1, -1, -1, 0, 1, 0, 0, 1, -1, -1])

        self.assertRaises(StopIteration, data.__next__)
