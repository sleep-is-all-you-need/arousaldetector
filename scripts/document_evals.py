from glob import glob
import json
import pickle
import sys
import pandas as pd
import yaml
from pyPhases import pdict

def loadConfig(json_path):
    with open(json_path, 'r') as file:
        config = json.load(file)
    return config

def findSegmentResults(evalFolder, prefix):
    dataId = evalFolder.split("/")[-1]
    dataIdSegments = "-".join(dataId.split("-")[:-2])
    evalResults = list(glob(f"data/evalResults{dataIdSegments}*"))
    results = {}
    if len(evalResults) > 1:
        raise Exception("there should only be one results")
    if len(evalResults) == 1:
        with open(evalResults[0], "rb") as f:
            data = pickle.load(f)
            results = {
                prefix + "auprc_seg": data[1]["auprc"],
                prefix + "f1_seg": data[1]["f1"],
            }
    return results

def findEvalResults(evalFolder):
    resultFile = "results.yml"
    yamlPath = evalFolder + "/" + resultFile
    # load yaml
    with open(yamlPath, 'r') as file:
        results = yaml.load(file, Loader=yaml.FullLoader) 


    segmentEval = "recordResultsSegment.csv"
    df = pd.read_csv(evalFolder + "/" + segmentEval)

    df["tn"] = 0
    df["fp"] = 0
    df["fn"] = 0
    df["tp"] = 0

    for row in df.iterrows():
        (tn, fn), (fp, tp) = eval(row["confusion"])
        df.loc[row["index"], "tn"] = tn
        df.loc[row["index"], "fn"] = fn
        df.loc[row["index"], "fp"] = fp
        df.loc[row["index"], "tp"] = tp

    return results

def find_different_keys(dict1, dict2, parent_key='', different_keys=None):
    if different_keys is None:
        different_keys = []

    for key in dict1.keys():
        value1 = dict1[key]
        value2 = dict2.get(key)

        if isinstance(value1, dict) and value2 and isinstance(value2, dict):
            find_different_keys(value1, value2, parent_key + '.' + key, different_keys)
        elif value1 != value2:
            different_keys.append(parent_key + '.' + key)

    return different_keys

def getNestedKeys(myPdict, path=""):
    keys = []

    for k, value in myPdict.items():
        if isinstance(value, dict):
            keys += getNestedKeys(value, path=path+"."+k)
        else:
            keys.append(path + "." + k)
    if path == "":
        keys = [k[1:] for k in keys]
    return keys

def main(pattern, targetCsv):
    evalFolder = 'eval/'
    configName = "project.config"
    
    evalResults = []

    # read best results
    evalFolders = list(glob(f"{evalFolder}*{pattern}*"))
    for folder in evalFolders:
        modelConfig = folder + "/" + configName
        try:
            evalResult = findEvalResults(folder)
            config = loadConfig(modelConfig)

            evalResults.append((pdict(config), evalResult))
        except Exception as e:
            print(e)
            pass
        # except 


    # resultFilter = lambda configResult: "epochs" in configResult[1] and configResult[1]["epochs"] is not None and configResult[1]["epochs"] > 3
    # results = list(filter(resultFilter, results))

    distinctKeyValues = {}
    for config, results in evalResults:
        keys = list(getNestedKeys(config))
        for key in keys:
            value = config[key.split(".")]
            if not isinstance(value, dict):
                if key not in distinctKeyValues:
                    distinctKeyValues[key] = set()
                
                if isinstance(value, list):
                    value = str(value)
                distinctKeyValues[key].add(value)
            else:
                pass
    # taylor configs
    rows = []
    configColumns = []
    distinctKeyValues = [k for k, v in distinctKeyValues.items() if len(v) > 1]
    distinctKeyValues.append("eventEval.tpStrat")
    for config, results in evalResults:
        relevantConfig = {k: config[k.split(".")]  if k.split(".") in config else None for k in distinctKeyValues}
        # relevantConfig = {k: config[k.split(".")] if k.split(".") in config else None for k, values in distinctKeyValues.items() if len(values) > 1}
        
        # relevantConfig = {}
        # for k, values in distinctKeyValues.items():
        #     if len(values) > 1:
        #         key_split = k.split(".")
        #         if key_split in config:
        #             relevantConfig[k] = config[key_split]
        #         else:
        #             relevantConfig[k] = None
        
        # configColumns = list(relevantConfig.keys())
        row = relevantConfig | results
        rows.append(row)
        
        # configColumns += list(relevantConfig.keys())
        row = relevantConfig | results
        # rows.append(row)
    configColumns = list(set(configColumns))
    
    df = pd.DataFrame(rows)
    df.to_csv(targetCsv, index=False)

if __name__ == '__main__':
    main(*sys.argv[1:])
