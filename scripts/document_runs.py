from glob import glob
import json
from pathlib import Path
import pickle
import pandas as pd
from pyPhases import pdict

def loadConfig(json_path):
    with open(json_path, 'r') as file:
        config = json.load(file)
    return config

def findBestResults(logFolderPath, config):
    # load from log.csv if its exists or from checkpoint-files inside the folder
    logFile = "log.csv"
    csvPath = Path(logFolderPath) / logFile
    metrics = config["trainingParameter"]["validationMetrics"]
    if csvPath.exists():
        prefix = "val_"
        # read Header and select best "auprc"-col
        df = pd.read_csv(str(csvPath))
        results = {m: df[prefix + m].max() for m in metrics}
        results["epochs"] = len(df)
    else:
        results = {m: 0 for m in metrics}
        maxEpoch = 0
        for checkPointFile in glob(logFolderPath +"/checkpoint*"):
            split = ".".join(checkPointFile.split("/")[-1].split(".")[:-1]).split("_")
            epoch = split[1]
            epochResults = split[2:]
            results = {m: max(results[m], float(epochResults[i])) for i, m in enumerate(metrics)}
            maxEpoch = max(maxEpoch, int(epoch))
            results["epochs"] = maxEpoch
    return results

def findSegmentResults(evalFolder, prefix):
    dataId = evalFolder.split("/")[-1]
    dataIdSegments = "-".join(dataId.split("-")[:-2])
    evalResults = list(glob(f"data/evalResults{dataIdSegments}*"))
    results = {}
    if len(evalResults) > 1:
        raise Exception("there should only be one results")
    if len(evalResults) == 1:
        with open(evalResults[0], "rb") as f:
            data = pickle.load(f)
            results = {
                prefix + "auprc_seg": data[1]["auprc"],
                prefix + "f1_seg": data[1]["f1"],
            }
    return results

def findEvalResults(evalFolder):
    resultFile = "recordResultsEvents.csv"
    csvPath = evalFolder + "/" + resultFile
    prefix = "eval"
    results = {}

    if Path(csvPath).exists():
        df = pd.read_csv(str(csvPath))
        values = list(df.columns)
        exampleRecordId = df.iloc[1, 0]
        if exampleRecordId.startswith("acq_"):
            prefix = "tsm_"
        elif exampleRecordId.startswith("shhs1"):
            prefix = "shhs1"
        elif exampleRecordId.startswith("mros"):
            prefix = "mros"
        elif exampleRecordId.startswith("mesa"):
            prefix = "mesa"
        if "examples" in values:
            values.remove("examples")

        df["f1"].mean()
        
        meanValues = ["f1", "auprc"]
        suffix = "_mean"
        results = {f"{prefix}_{m}_{suffix}": df[m].mean() for m in meanValues}
        hasIndex = "indexArousal-prediction" in values
        if "countArousal-software" in values:
            df["arCount-diff"] = df["countArousal-prediction"] - df["countArousal-software"]
            
            if hasIndex:
                df["arI-diff"] = df["indexArousal-prediction"] - df["indexArousal-software"]
        else:
            df["arCount-diff"] = df["countArousal-prediction"] - df["countArousal-truth"]
            if hasIndex:
                df["arI-diff"] = df["indexArousal-prediction"] - df["indexArousal-truth"]

        medValues = ["eventCountDiff", "arCount-diff"]
        
        if hasIndex:
            medValues.append("arI-diff")

        suffix = "_med"
        results |= {f"{prefix}_{m}_{suffix}": df[m].median() for m in medValues}

    
    segmentResults = findSegmentResults(evalFolder, prefix)
    results |= segmentResults

    return results

def find_different_keys(dict1, dict2, parent_key='', different_keys=None):
    if different_keys is None:
        different_keys = []

    for key in dict1.keys():
        value1 = dict1[key]
        value2 = dict2.get(key)

        if isinstance(value1, dict) and value2 and isinstance(value2, dict):
            find_different_keys(value1, value2, parent_key + '.' + key, different_keys)
        elif value1 != value2:
            different_keys.append(parent_key + '.' + key)

    return different_keys

def getNestedKeys(myPdict, path=""):
    keys = []

    for k, value in myPdict.items():
        if isinstance(value, dict):
            keys += getNestedKeys(value, path=path+"."+k)
        else:
            keys.append(path + "." + k)
    if path == "":
        keys = [k[1:] for k in keys]
    return keys

def main():
    logDirectory = 'logs/'
    evalFolder = 'eval/'
    configName = "model.config"
    
    logResults = []
    # iterate over all log folders
    for folder in glob(logDirectory + '/*'):
        # get Config
        modelConfig = folder + "/" + configName
        if not Path(modelConfig).exists():
            subFiles = list(glob(folder + "/*"))
            if len(subFiles) > 0:
                print("no config found for %s: "%folder)
                print("files: %s"%(subFiles))
            continue
        config = loadConfig(modelConfig)
        
        # read best results
        bestResults = findBestResults(folder, config)
        modelStateId = folder[len(logDirectory):]
        config["modelStateId"] = modelStateId
        evalFolders = list(glob(f"{evalFolder}*{modelStateId}*"))
        config["evalFolders"] = evalFolders
        for f in evalFolders:
            bestResults |= findEvalResults(f)

        logResults.append((pdict(config), bestResults))

    resultFilter = lambda configResult: "epochs" in configResult[1] and configResult[1]["epochs"] is not None and configResult[1]["epochs"] > 3
    logResults = list(filter(resultFilter, logResults))

    distinctKeyValues = {}
    for config, results in logResults:
        keys = list(getNestedKeys(config))
        for key in keys:
            value = config[key.split(".")]
            if not isinstance(value, dict):
                if key not in distinctKeyValues:
                    distinctKeyValues[key] = set()
                
                if isinstance(value, list):
                    value = str(value)
                distinctKeyValues[key].add(value)
            else:
                pass
    # taylor configs
    rows = []
    for config, results in logResults:
        relevantConfig = {k: config[k.split(".")] if k.split(".") in config else None for k, values in distinctKeyValues.items() if len(values) > 1}
        row = relevantConfig | results
        rows.append(row)
    df = pd.DataFrame(rows)
    df.to_csv(logDirectory + "/logOverview.csv", index=False)

if __name__ == '__main__':
    main()
